/**
 * Created by ChrisMatos on 11/7/2016.
 */

public with sharing class SalesTrackingController {

    public List<SelectOption> monthOptions{get;set;}
    public Integer monthChoice{get;set;}
    public Map<Integer, String> monthMap;
    public List<String> monthOptionStrings{get;set;}

    public List<User> users{get;set;}
    public Map<Id, User> userRoleById;
    public Map<Id, Map<Integer, List<Sales_Call__c>>> salesCallsByMonthByUserIds;
    public Map<Id, UserWrapper> userWrappersByUserId;
    public List<UserWrapper> userTree{get;set;}
    public UserWrapper rootWrapper{get;set;}

    public Date startDate;
    public Date endDate;
    public Id currentUserRoleId{get;set;}
    private Id currentUserProfileId;
    private Id adminProfileId;
    private Id currentUserId{get;set;}
    public String currentUserRoleName{get;set;}
    public UserWrapper currentParent{get;set;}
    private Integer NUM_MONTHS;
    private Boolean isDirector{get;set;}


    public SalesTrackingController(){

        NUM_MONTHS = (Integer)Revenue_Call_Config__c.getValues('Default').Months_Displayed__c;

        if(NUM_MONTHS == NULL || NUM_MONTHS <= 0 || NUM_MONTHS > 12){
            userTree = new List<UserWrapper>();
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Custom Setting is invalid, Go to Build->Develop->Custom Settings to change'));
            return;
        }

        currentUserId = UserInfo.getUserId();
        currentUserProfileId = UserInfo.getProfileId();
        currentUserRoleId = UserInfo.getUserRoleId();





        Id catmanRoleId = [SELECT id FROM UserRole WHERE DeveloperName = 'Cat_Man'].id;

        adminProfileId = [SELECT id FROM Profile WHERE Name = 'System Administrator'].id;

        List<UserRole> directorRoles = [SELECT id FROM UserRole
        WHERE DeveloperName='Sales_Director' OR
        DeveloperName='Regional_East_Director' OR
        DeveloperName='Regional_West_Director' OR
        DeveloperName='Rergional_Midwest_Director'];

        isDirector = FALSE;
        for(UserRole ur : directorRoles){
            if(currentUserRoleId == ur.id){
                isDirector = TRUE;
            }

        }



        if(currentUserProfileId == adminProfileId || currentUserRoleId == catmanRoleId || isDirector ){
            currentUserRoleId = [SELECT id FROM UserRole WHERE DeveloperName = 'Pillar' LIMIT 1].id;
            currentUserId = [SELECT id FROM User WHERE UserRoleId = :currentUserRoleId LIMIT 1].id;
        }


        if(currentUserRoleId != null){
            currentUserRoleName = [SELECT Name FROM UserRole WHERE id = :currentUserRoleId LIMIT 1].Name;
        }

        populateUsers();
        setupMonthOptions();

        System.debug(startDate);
        System.debug(endDate);
        //default month choice
        if(monthOptions.size() > 0){
            monthChoice = Integer.valueOf(monthOptions.get(0).getValue());
        }
        salesCallsByMonthByUserIds = getSalesCallMap();
        System.debug(salesCallsByMonthByUserIds);
        System.debug(monthChoice);


        userWrappersByUserId = new Map<Id, UserWrapper>();
        for(User u : users){
            UserWrapper uw = new UserWrapper();
            uw.theUser= u;
//            uw.theUser.UserRoleId = currentUserRoleId;
            if(salesCallsByMonthByUserIds.containsKey(u.id)){
                uw.salesCalls = salesCallsByMonthByUserIds.get(u.id).get(monthChoice);
            }
            uw.populateAllOther();
            userWrappersByUserId.put(u.id, uw);
        }
        buildHierachy();
        System.debug(userTree);
       
    }
    public void setupMonthOptions(){
        initMonthMap();
        monthOptions = getMonthOptions();
    }
    public void initMonthMap(){
        monthMap = new Map<Integer, String>();
        monthMap.put(1,'January');
        monthMap.put(2,'February');
        monthMap.put(3,'March');
        monthMap.put(4,'April');
        monthMap.put(5,'May');
        monthMap.put(6,'June');
        monthMap.put(7,'July');
        monthMap.put(8,'August');
        monthMap.put(9,'September');
        monthMap.put(10,'October');
        monthMap.put(11,'November');
        monthMap.put(12,'December');
    }
    public List<SelectOption> getMonthOptions(){
        List<SelectOption> options = new List<SelectOption>();
        startDate = Date.today().toStartOfMonth();
        Date currentDate = startDate;
        for(Integer i = 0; i < NUM_MONTHS; i++){
            currentDate = startDate.addMonths(i);
            options.add(new SelectOption(String.valueOf(currentDate.month()),monthMap.get(currentDate.month())));
        }
        endDate = currentDate.addMonths(1).addDays(-1);
        return options;
    }

    public void populateUsers(){
        users = new List<User>();

        if(currentUserRoleId != null){
            List<UserRole> childrenRoles = [SELECT id,ParentRoleId FROM UserRole WHERE ParentRoleId = :currentUserRoleId];
            List<UserRole> grandChildrenRoles = [SELECT id FROM UserRole WHERE ParentRoleId  IN :childrenRoles];
            List<UserRole> greatGrandChildrenRoles = [SELECT id FROM UserRole WHERE ParentRoleId IN :grandChildrenRoles];

            List<User> currentUser = [SELECT id,Name, UserRoleId, ManagerId,
                (SELECT id, Name FROM ManagedUsers) FROM User WHERE Id = :currentUserId];
            users.addAll(currentUser);
            System.debug(users);

            List<User> childrenUsers = [SELECT id, Name, UserRoleId, ManagerId,
                (SELECT id, Name FROM ManagedUsers)
                FROM User WHERE UserRoleId IN :childrenRoles];
            List<User> grandChildrenUsers = [SELECT id, Name, UserRoleId,ManagerId,
                (SELECT id, Name FROM ManagedUsers)
                FROM User WHERE UserRoleId IN :grandChildrenRoles];
            List<User> greatGrandChildrenUsers = [SELECT id, Name, UserRoleId, ManagerId,
                (SELECT id, Name FROM ManagedUsers)
                FROM User WHERE UserRoleId IN :greatGrandChildrenRoles];

            users.addAll(childrenUsers);
            users.addAll(grandChildrenUsers);
            users.addAll(greatGrandChildrenUsers);

        }

        System.debug(users);


    }
    public Map<Id, Map<Integer, List<Sales_Call__c>>> getSalesCallMap(){
        Map<Id, Map<Integer, List<Sales_Call__c>>> salesCallsByAccountOwnerIdByMonth = new Map<Id, Map<Integer, List<Sales_Call__c>>>();

        if(isDirector){
            salesCallsByAccountOwnerIdByMonth = SalesTrackingDirectorController.getSalesCallMapAllAccounts(startDate, endDate);
            System.debug(salesCallsByAccountOwnerIdByMonth);
        } else {
            for(Sales_Call__c sc : [SELECT id, LE_Sales_Call__c, Plan__c,Replan__c, Latest_Estimate__c, Replan_Minus_Sales_Call__c, LE_Demand_Forecast__c, Customer__r.Name, S_O__c, Date__c, Customer__r.OwnerId, LE_Minus_Sales_Call__c, Customer__r.Is_All_Other__c FROM Sales_Call__c WHERE (Date__c >= :startDate AND Date__c < :endDate) AND Customer__r.OwnerId IN :users ORDER BY LE_Sales_Call__c DESC]){
                if(!salesCallsByAccountOwnerIdByMonth.containsKey(sc.Customer__r.OwnerId)){
                    salesCallsByAccountOwnerIdByMonth.put(sc.Customer__r.OwnerId, new Map<Integer, List<Sales_Call__c>>());
                }
                if(!salesCallsByAccountOwnerIdByMonth.get(sc.Customer__r.OwnerId).containsKey(sc.Date__c.month())){
                    salesCallsByAccountOwnerIdByMonth.get(sc.Customer__r.OwnerId).put(sc.Date__c.month(), new List<Sales_Call__c>());
                }
                if(sc.Latest_Estimate__c == null){
                    sc.Latest_Estimate__c = sc.LE_Demand_Forecast__c;
                }
                salesCallsByAccountOwnerIdByMonth.get(sc.Customer__r.OwnerId).get(sc.Date__c.month()).add(sc);
            }
            System.debug(salesCallsByAccountOwnerIdByMonth);
        }
        return salesCallsByAccountOwnerIdByMonth;
    }


    public void buildHierachy(){
        userTree = new List<UserWrapper>();
        for(UserWrapper wrapper : userWrappersByUserId.values()){

            //if manager == null, add to usertree

            if ( wrapper.theUser.UserRoleId == currentUserRoleId) {
                userTree.add(wrapper);
            }
        }
        for(UserWrapper wrapper : userTree){
           addChildren(wrapper);
        }

        
    }
    public void addChildren(UserWrapper wrapper){
        if(wrapper == null || wrapper.theUser.ManagedUsers.size() == 0){
            return;
        }
        for(User childUser : wrapper.theUser.ManagedUsers){
//            if(wrapper.childrenWrappers == null){
//                wrapper.childrenWrappers = new List<UserWrapper>();
//            }


            if(userWrappersByUserId.containsKey(childUser.id)){
                wrapper.childrenWrappers.add(userWrappersByUserId.get(childUser.id));
            }

            
        }
        for(UserWrapper childWrapper : wrapper.childrenWrappers){
            addchildren(childWrapper);
        }
    }
    

    public PageReference save(){
        List<Sales_Call__c> salesCallsToUpdate = getSalesCallsFromWrappers(userTree);

        try{
            System.debug(salesCallsToUpdate);
            update salesCallsToUpdate;
            salesCallsByMonthByUserIds = getSalesCallMap();
            updateSalesCalls(userTree);

        }
        catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'We encountered an Error'));
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            System.debug(e.getMessage());
            System.debug(e.getStackTraceString());
            return null;
        }
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Records Updated Successfully'));
        return null;
    }
    public List<Sales_Call__c> getSalesCallsFromWrappers(List<UserWrapper> wrapppers){
        List<Sales_Call__c> salesCalls = new List<Sales_Call__c>();
        if(wrapppers == null){
            return salesCalls;
        }
        for(UserWrapper uw : wrapppers){
            salesCalls.addAll(getSalesCallsFromWrappers(uw.childrenWrappers));
            if(salesCallsByMonthByUserIds.containsKey(uw.theUser.id)){
                for(List<Sales_Call__c> calls : salesCallsByMonthByUserIds.get(uw.theUser.id).values()){
                    for(Sales_Call__c call : calls){
                        if(call.Latest_Estimate__c != null){
                            call.Latest_Estimate__c = call.Latest_Estimate__c.setScale(0);
                        }

                    }
                    salesCalls.addAll(calls);
                }
                if(uw.allOtherGroup != null && uw.allOtherGroup.salesCalls != null){
                    for(Sales_Call__c sc : uw.allOtherGroup.salesCalls){
                        Double multiplier;
                        if(uw.allOtherGroup.leCall == 0){
                            multiplier = Double.valueOf(1) / Double.valueOf(uw.allOtherGroup.salesCalls.size());
                        }
                        else{
                            if(sc.LE_Sales_Call__c != null){
                                multiplier=  sc.LE_Sales_Call__c / uw.allOtherGroup.leCall;
                            }
                            else{
                                multiplier = 0;
                            }

                        }

                        sc.Latest_Estimate__c = uw.allOtherGroup.latestEstimate * multiplier;
                        sc.Latest_Estimate__c = sc.Latest_Estimate__c.setScale(0);
                    }
                    salesCalls.addAll(uw.allOtherGroup.salesCalls);
                }
            }


        }
        return salesCalls;
    }
    public PageReference cancel(){
        return new PageReference('/');
    }

    public void createRowsFromUserWrappers(AttachmentBodyWrapper bodyWrap, List<UserWrapper> userWrappers){
        if(userWrappers == null){
            return;
        }
        for(UserWrapper uw : userWrappers){
            if(uw.salesCalls != null){
                for(Sales_Call__c call : uw.salesCalls){
                    bodyWrap.newRow(call, uw.theUser.Name);
                    bodyWrap.appendNewLine();
                }
            }
            if(uw.allOtherGroup != null){
                bodyWrap.newRow(uw.allOtherGroup, uw.theUser.Name);
                bodyWrap.appendNewLine();
            }
            createRowsFromUserWrappers(bodyWrap, uw.childrenWrappers);
        }


    }
    public PageReference exportAsCSV(){

        if(userTree == null || userTree.size() == 0){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Nothing to export'));
            return null;
        }


        AttachmentBodyWrapper bodyWrap = new AttachmentBodyWrapper();


        createRowsFromUserWrappers(bodyWrap, userTree);
        if(bodyWrap.rows == 0){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'No rows to export'));
            return null;
        }
        System.debug(bodyWrap.attachmentBody);
        Savepoint sp = Database.setSavepoint();
        try{
            CSV_Export__c csv_export = new CSV_Export__c();
            insert csv_export;
            Attachment att = new Attachment();
            att.Name = 'Sales Tracker Export:' + Date.today().format() + '.csv';
            att.Body = Blob.valueOf(bodyWrap.attachmentBody);
            att.ParentId = csv_export.id;
            att.ContentType = 'text/csv';
            insert att;

            return new PageReference('/servlet/servlet.FileDownload?file=' + att.id);
        }
        catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getMessage()));
            System.debug(e.getMessage());
            System.debug(e.getStackTraceString());
            Database.rollback(sp);
            return null;
        }
    }

    public class AttachmentBodyWrapper{
        final String USER_HEADER= 'User';
        final String ACCOUNT_HEADER= 'Customer Group';
        List<String> headerValues;
        public String attachmentBody;
        public Integer rows;

        public AttachmentBodyWrapper(){
            headerValues = new List<String>{USER_HEADER,ACCOUNT_HEADER, 'Replan__c', 'LE_Demand_Forecast__c','S_O__c', 'Latest_Estimate__c','LE_Minus_Sales_Call__c','Replan_Minus_Sales_Call__c', 'Date__c'};
            attachmentBody = '';
            attachmentBody += String.join(headerValues, ',');
            rows = 0;
            appendNewLine();
        }
        public void newRow(Sales_Call__c call, String userName){
            if(call == null){
                return;
            }
            for(String field : headerValues){
                if(field == USER_HEADER){
                    if(userName != null){
                        addValue(userName);
                    }
                }
                else if(field == ACCOUNT_HEADER){
                    if(call.Customer__r.Name != null){
                        addValue(call.Customer__r.Name);
                    }
                }
                else{
                    if(call.get(field) != null){
                        attachmentBody += String.valueOf(call.get(field)).escapeCsv();
                    }
                }
                attachmentBody += ',';
            }
            rows++;
        }
        public void addValue(String value){
            if(value != null){
                attachmentBody += value.escapeCsv();
            }
        }
        public void newRow(UserWrapper.AllOtherGroupWrapper allOther, String userName){
            if(allOther == null){
                return;
            }
            for(String field : headerValues){
                if(field == USER_HEADER){
                    if(userName != null){
                        addValue(userName);
                    }
                }
                else if(field == ACCOUNT_HEADER){
                    addValue('All Other');
                }
                else if(field == 'Replan__c'){
                    if(allOther.plan != null){
                        addValue(String.valueOf(allOther.plan));
                    }
                }
                else if(field == 'LE_Sales_Call__c'){
                    if(allOther.leCall != null){
                        addValue(String.valueOf(allOther.leCall));

                    }
                }
                else if(field == 'LE_Demand_Forecast__c'){
                    if(allOther.leCall != null){
                        addValue(String.valueOf(allOther.ledemand));

                    }
                }
                else if(field == 'Latest_Estimate__c'){
                    if(allOther.latestEstimate != null){
                        addValue(String.valueOf(allOther.latestEstimate));
                    }
                }
                else if(field == 'LE_Minus_Sales_Call__c'){
                    if(allOther.leMinus != null){
                        addValue(String.valueOf(allOther.leMinus));
                    }
                }
                else if(field == 'Date__c'){
                    if(allOther.salesCalls != null && allOther.salesCalls.size() > 0){
                        //hack, asssuming all calls in All Other bucket have same date...
                        Sales_Call__c call = allOther.salesCalls.get(0);
                        if(call.Date__c != null){
                            addValue(String.valueOf(call.Date__c) + ' 0:00');
                        }
                    }
                }
                else if(field == 'S_O__c'){
                    if(allOther.currentSO != null){
                        addValue(String.valueOf(allOther.currentSO));
                    }
                }
                attachmentBody += ',';
            }
        }
        public void appendNewLine(){
            attachmentBody += '\n';
        }
    }

    public void changeMonth(){
        System.debug(monthChoice);
        updateSalesCalls(userTree);
        System.debug('month changed');
    }
    public void updateSalesCalls(List<UserWrapper> wrappers){
        if(wrappers == null || wrappers.size() == 0){
            return;
        }

        System.debug(salesCallsByMonthByUserIds);
        System.debug(wrappers.size());
        for(UserWrapper parent : wrappers){
            if(salesCallsByMonthByUserIds.containsKey(parent.theUser.id)){
                System.debug('HERE');
                //have to put back All Other sales calls before changing months
                if(parent.allOtherGroup != null && parent.allOtherGroup.salesCalls != null){
                    for(Sales_Call__c call : parent.allOtherGroup.salesCalls){
                        if(parent.salesCalls == null){
                            parent.salesCalls = new List<Sales_Call__c>();
                        }
                        parent.salesCalls.add(call);
                    }
                    parent.allOtherGroup = null;
                }
                parent.salesCalls = salesCallsByMonthByUserIds.get(parent.theUser.id).get(monthChoice);
                if(parent.salesCalls != null){
                    parent.populateAllOther();
                }

            }
            updateSalesCalls(parent.childrenWrappers);
        }

    }
    public void toggleDirector(){
        if(currentParent != null){
            currentParent.expanded = !currentParent.expanded;
        }
    }
    public Decimal planTotal{
        get{
            if(planTotal != null){return planTotal;}
            Decimal theTotal = 0;
            for(UserWrapper uw : userTree){
                theTotal +=  uw.getChildTotalPlan();
            }
            return theTotal;
        }
    }
    public Decimal LETotal{
        get{
            if(LETotal != null){return LETotal;}
            Decimal theTotal = 0;
            for(UserWrapper uw : userTree){
                theTotal +=  uw.getChildTotalLESalesCall();
            }
            return theTotal;
        }
    }
    public Decimal LEDemandTotal{
        get{
            if(LEDemandTotal != null){return LEDemandTotal;}
            Decimal theTotal = 0;
            for(UserWrapper uw : userTree){
                theTotal +=  uw.getChildTotalLEDemand();
            }
            return theTotal;
        }
    }
    public Decimal SOTotal{
        get{
            if(SOTotal != null){return SOTotal;}
            Decimal theTotal = 0;
            for(UserWrapper uw : userTree){
                theTotal +=  uw.getChildTotalCurrentSO();
            }
            return theTotal;
        }
    }
    public Decimal latestEstimateTotal{
        get{
            if(latestEstimateTotal != null){return latestEstimateTotal;}
            Decimal theTotal = 0;
            for(UserWrapper uw : userTree){
                theTotal +=  uw.getChildTotalLatestEstimate();
            }
            return theTotal;
        }
    }
    public Decimal leMinusCallTotal{
        get{
            if(leMinusCallTotal != null){return leMinusCallTotal;}
            Decimal theTotal = 0;
            for(UserWrapper uw : userTree){
                theTotal +=  uw.getChildTotalLEMinusCall();
            }
            return theTotal;
        }
    }
    public Decimal replanMinusCallTotal{
        get{
            if(replanMinusCallTotal != null){return replanMinusCallTotal;}
            Decimal theTotal = 0;
            for(UserWrapper uw : userTree){
                theTotal +=  uw.getChildTotalReplanMinusCall();
            }
            return theTotal;
        }
    }


}