/**
 * Created by AlexSanborn on 6/5/2017.
 */

@isTest
public with sharing class RetailPlanControllerTest {

    @TestSetup
    public static void testSetup(){
        Product2 prod1 = new Product2(Name='Sour gummy worms', IsActive=TRUE, Annual_Revenue__c=1000);
        Product2 prod2 = new Product2(Name='Sour gummy octopi', IsActive=TRUE, Annual_Revenue__c=2000);
        Product2 prod3 = new Product2(Name='Sour gummy peaches', IsActive=TRUE, Annual_Revenue__c=3000);

        List<Product2> newProducts = new List<Product2>{prod1, prod2, prod3};
        insert newProducts;

        RecordType retailer = [SELECT id FROM RecordType WHERE DeveloperName='Retailer'];
        Account newRetailer = new Account(Name='theRetailerTest', RecordTypeId=retailer.id);
        insert newRetailer;

        Opportunity opp1 = new Opportunity(Name='Sour gummy worms', Item__c=prod1.id, StageName='Open', CloseDate=Date.today(), AccountId=newRetailer.id, Base_Store_Count__c=5, Base_Reset_Date__c=Date.today().addMonths(-3), Base_Revenue__c=7000, Plan_Revenue__c=8000, TDP__c=500, Plan_Store_Count__c=9, Plan_Reset_Date__c=Date.today().addMonths(4));
        Opportunity opp2 = new Opportunity(Name='Sour gummy octopi', Item__c=prod2.id, StageName='Open', CloseDate=Date.today(), AccountId=newRetailer.id, Base_Store_Count__c=4, Base_Reset_Date__c=Date.today().addMonths(-2), Base_Revenue__c=3000, Plan_Revenue__c=4000, TDP__c=0);
        Opportunity opp3 = new Opportunity(Name='Sour gummy peaches', Item__c=prod3.id, StageName='Open', CloseDate=Date.today(), AccountId=newRetailer.id, Base_Store_Count__c=0, Base_Reset_Date__c=Date.today().addMonths(-1), Base_Revenue__c=1000, Plan_Revenue__c=2000, Plan_Distribution_Status__c = 'No Distribution');

        List<Opportunity> newOpps = new List<Opportunity>{opp1, opp2, opp3};
        insert newOpps;
    }

    @isTest
    public static void loadOpportunities(){
        Account theRetailer = [SELECT id FROM Account WHERE Name='theRetailerTest' LIMIT 1].get(0);
        ApexPages.StandardController accountController = new ApexPages.StandardController(theRetailer);
        RetailerPlanController controller = new RetailerPlanController(accountController);
        System.assertEquals(3, controller.addedOpportunities.size());
    }


    @isTest
    public static void addsOpportunityLine(){
        Account theRetailer = [SELECT id FROM Account WHERE Name='theRetailerTest' LIMIT 1].get(0);
        ApexPages.StandardController accountController = new ApexPages.StandardController(theRetailer);
        RetailerPlanController controller = new RetailerPlanController(accountController);
        System.assertEquals(3, controller.addedOpportunities.size());
        controller.addOpportunityLine();
        System.assertEquals(4, controller.addedOpportunities.size());
    }

    @isTest
    public static void upsertsOpportunities(){
        Product2 prod4 = new Product2(Name='Jawbreaker', IsActive=TRUE);
        insert prod4;

        Account theRetailer = [SELECT id FROM Account WHERE Name='theRetailerTest' LIMIT 1].get(0);
        ApexPages.StandardController accountController = new ApexPages.StandardController(theRetailer);
        RetailerPlanController controller = new RetailerPlanController(accountController);
        controller.addOpportunityLine();
        controller.addedOpportunities.get(3).opp.Item__c = prod4.id;

        PageReference retailerPage = Page.RetailerPlan;
        Test.setCurrentPage(retailerPage);
        ApexPages.currentPage().getParameters().put('oppToUpdate', '3');

        controller.updateOppWithSKU();
        controller.save();
        Integer currentOpportunities = [SELECT COUNT() FROM Opportunity WHERE AccountId = :theRetailer.id];
        System.assertEquals(4, currentOpportunities);
    }


    @isTest
    public static void deleteOpportunity(){
        Account theRetailer = [SELECT id FROM Account WHERE Name='theRetailerTest' LIMIT 1].get(0);
        ApexPages.StandardController accountController = new ApexPages.StandardController(theRetailer);
        RetailerPlanController controller = new RetailerPlanController(accountController);
        System.assertEquals(3, controller.addedOpportunities.size());

        PageReference retailerPage = Page.RetailerPlan;
        Test.setCurrentPage(retailerPage);
        ApexPages.currentPage().getParameters().put('index', '0');

        controller.deleteOpportunity();
        System.assertEquals(2, controller.addedOpportunities.size());
    }

    @isTest
    public static void updateLineValues(){
        Account theRetailer = [SELECT id FROM Account WHERE Name='theRetailerTest' LIMIT 1].get(0);
        ApexPages.StandardController accountController = new ApexPages.StandardController(theRetailer);
        RetailerPlanController controller = new RetailerPlanController(accountController);
        System.assertEquals('Current', controller.addedOpportunities.get(0).opp.Base_Distribution_Status__c);
        System.assertEquals(7000, controller.addedOpportunities.get(0).opp.Base_Revenue__c);

        PageReference retailerPage = Page.RetailerPlan;
        Test.setCurrentPage(retailerPage);

        controller.addedOpportunities.get(0).opp.Base_Distribution_Status__c = 'Gain';
        ApexPages.currentPage().getParameters().put('oppToUpdate', '0');
        controller.updateRevenueValues();
        System.assertEquals('Gain', controller.addedOpportunities.get(0).opp.Base_Distribution_Status__c);
        System.assertEquals(4583.333333333333333333333333333334, controller.addedOpportunities.get(0).opp.Base_Revenue__c);
        System.assertEquals('Current', controller.addedOpportunities.get(0).opp.Plan_Distribution_Status__c);
        System.assertEquals(9000, controller.addedOpportunities.get(0).opp.Plan_Revenue__c);

        controller.addedOpportunities.get(0).opp.Base_Distribution_Status__c = 'Loss';
        ApexPages.currentPage().getParameters().put('oppToUpdate', '0');
        controller.updateRevenueValues();
        System.assertEquals('Loss', controller.addedOpportunities.get(0).opp.Base_Distribution_Status__c);
        System.assertEquals(1250, controller.addedOpportunities.get(0).opp.Base_Revenue__c);
        System.assertEquals('No Distribution', controller.addedOpportunities.get(0).opp.Plan_Distribution_Status__c);
        System.assertEquals(0, controller.addedOpportunities.get(0).opp.Plan_Store_Count__c);
        System.assertEquals(null, controller.addedOpportunities.get(0).opp.Plan_Reset_Date__c);
        System.assertEquals(0, controller.addedOpportunities.get(0).opp.Plan_Revenue__c);

        controller.addedOpportunities.get(0).opp.Base_Distribution_Status__c = 'No Distribution';
        ApexPages.currentPage().getParameters().put('oppToUpdate', '0');
        controller.updateRevenueValues();
        System.assertEquals('No Distribution', controller.addedOpportunities.get(0).opp.Base_Distribution_Status__c);
        System.assertEquals(0, controller.addedOpportunities.get(0).opp.Base_Store_Count__c);
        System.assertEquals(null, controller.addedOpportunities.get(0).opp.Base_Reset_Date__c);
        System.assertEquals(0, controller.addedOpportunities.get(0).opp.Base_Revenue__c);
        System.assertEquals('No Distribution', controller.addedOpportunities.get(0).opp.Plan_Distribution_Status__c);
        System.assertEquals(0, controller.addedOpportunities.get(0).opp.Plan_Store_Count__c);
        System.assertEquals(null, controller.addedOpportunities.get(0).opp.Plan_Reset_Date__c);
        System.assertEquals(0, controller.addedOpportunities.get(0).opp.Plan_Revenue__c);
    }

    @isTest
    public static void sortsColumns(){
        Account theRetailer = [SELECT id FROM Account WHERE Name='theRetailerTest' LIMIT 1].get(0);
        ApexPages.StandardController accountController = new ApexPages.StandardController(theRetailer);
        RetailerPlanController controller = new RetailerPlanController(accountController);

        PageReference retailerPage = Page.RetailerPlan;
        Test.setCurrentPage(retailerPage);

        controller.orderBy = 'Item__r.Name';
        controller.sort();
        System.assertEquals('Sour gummy octopi', controller.addedOpportunities.get(0).opp.Item_Description__c);

        controller.sort();
        System.assertEquals('Sour gummy worms', controller.addedOpportunities.get(0).opp.Item_Description__c);

        controller.sort();
        System.assertEquals('Sour gummy octopi', controller.addedOpportunities.get(0).opp.Item_Description__c);
    }
}