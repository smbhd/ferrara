/**
 * Created by ChrisMatos on 2/6/2017.
 */

@IsTest
public with sharing class DistributionControllerTest {
    public static testMethod void itShouldPullOpportunityData(){

        Account a = new Account();
        a.Name = 'Test Account';
        a.RecordTypeId = [SELECT id FROM RecordType WHERE SobjectType = 'Account' AND DeveloperName = 'Retailer'].id;
        insert a;

        Product2 item = new Product2();
        item.Name = 'Test Item';
        item.UPC__c = '12345';
        item.IsActive = true;
        insert item;

        Opportunity opp = new Opportunity();
        opp.Name = 'Test opp';
        opp.AccountId = a.id;
        opp.Item__c = item.id;
        opp.StageName = 'Pending';
        opp.CloseDate = Date.today();
        opp.TDP__c = 100;
        opp.Planned_TDP__c = 200;
        opp.PY_TDP__c = 300;
        insert opp;

        DistributionController dc = new DistributionController();
        dc.nextPage();
        dc.previousPage();
        Double count = dc.retailerWrappers.get(0).leTDPtotal;
        dc.targetPage = 1;
        dc.goToPage();

        System.assertEquals(100, dc.leTDPTotal);
        System.assertEquals(200, dc.planTotal);
        System.assertEquals(300, dc.pyTotal);


    }
}