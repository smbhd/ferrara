/**
 * Created by ChrisMatos on 2/22/2017.
 */

@IsTest
public with sharing class OpportunityTDPControllerTest {

    @TestSetup
    public static void testSetup(){
        
		Product2 item = new Product2();
        item.Name = 'Must Item';
        item.Heat_Map_Group__c = 'Must Item';
        item.Stock__c = 'Must';
        item.Heat_Map_Pack_Group__c = 'Peg';
        item.Item_Code__c = String.valueOf(Math.mod(Math.random().intValue(), 1000));
        item.IsActive = True;
        insert item;
        
        Account retailer = new Account();
        retailer.RecordTypeId = [SELECT id FROM RecordType WHERE SobjectType = 'Account' AND DeveloperName = 'Retailer'].id;
        retailer.Name = 'Test Retailer';
        insert retailer;
        Opportunity opp = new Opportunity();
        opp.RecordTypeId = [SELECT id FROM RecordType WHERE SobjectType = 'Opportunity' AND DeveloperName = 'Retailer_Opportunity'].id;
        opp.Name = 'Retailer Opp';
        opp.AccountId = retailer.id;
        opp.StageName = 'Pending';
        opp.Planned_TDP__c = 50;
        opp.PY_TDP__c = 60;
        opp.Amount = 0;
        opp.CloseDate = Date.today();
        opp.Item__c = item.id;

        insert opp;
    }

    @IsTest
    public static void itShouldDisplayAllRetailerOpps(){
        OpportunityTDPController controller = new OpportunityTDPController();
        controller.save();
        controller.addOpportunityLine();
        Opportunity newOpp ;
        for(Opportunity opp : controller.currentOpps){
            if(opp.id == null){
                newOpp = opp;
                break;
            }
        }
        Account acc = [SELECT id FROM Account WHERE Name = 'Test Retailer'];
        newOpp.AccountId = acc.id;
        newOpp.CloseDate = Date.today();
        newOpp.StageName = 'Pending';
        newOpp.Item__c = [SELECT id FROM Product2 WHERE Name = 'Must Item' LIMIT 1].id;
        controller.save();

        System.assertEquals(2, [SELECT id FROM Opportunity].size());

        System.assertNotEquals('PLACEHOLDER', [SELECT Name FROM Opportunity WHERE Id = :newOpp.id LIMIT 1].Name);

    }
    @isTest
    public static void itShouldExportAsCSV(){
        OpportunityTDPController controller = new OpportunityTDPController();
        controller.exportCSV();
        System.assertEquals(1, [SELECT id FROM CSV_Export__c].size());
    }
    @IsTest
    public static void itShouldSupportPagination(){
        OpportunityTDPController controller = new OpportunityTDPController();
        controller.nextPage();
        controller.previousPage();
        controller.targetPage = 1;
        controller.goToPage();

    }
    @IsTest
    public static void itShouldSupportMultiplePages(){
        List<Opportunity> opportunities = new List<Opportunity>();
        Account acc = [SELECT id FROM Account WHERE Name = 'Test Retailer'];
        Id oppRecordType = [SELECT id FROM RecordType WHERE DeveloperName = 'Retailer_Opportunity' LIMIT 1].id;
        Id itemId = [SELECT id FROM Product2 WHERE Name = 'Must Item' LIMIT 1].id;
        for(Integer i = 0; i < OpportunityTDPController.PAGE_SIZE + 1; i++){
            Opportunity opp = new Opportunity();
            opp.RecordTypeId = oppRecordType;
            opp.Name = 'Test Opp ' + i;
            opp.StageName = 'Pending';
            opp.CloseDate = Date.today().addDays(i);
            opp.Item__c = itemId;
            opp.TDP__c = 10;
            opp.AccountId = acc.id;
            opportunities.add(opp);
        }
        insert opportunities;
        OpportunityTDPController controller = new OpportunityTDPController();
        System.assertEquals(2, controller.availablePages);
        controller.nextPage();
        System.assertEquals(2, controller.currentPage);
        controller.previousPage();
        System.assertEquals(1, controller.currentPage);
        controller.targetPage = 2;
        controller.goToPage();
        System.assertEquals(2, controller.currentPage);

    }
}