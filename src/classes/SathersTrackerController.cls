/**
 * Created by ChrisMatos on 3/6/2017.
 */

public with sharing class SathersTrackerController {
    private Account[] allAccounts{get;set;}
    public Account[] currentAccounts{get;set;}

    private String baseQuery{get;set;}
    public SelectOption[] ownerOptions{get;set;}
    public SelectOption[] managerOptions{get;set;}
    public String orderBy{get;set;}
    public String previousOrderBy{get;set;}
    public Boolean isAsc{get;set;}

    public Integer currentPage {get; set;}
    public List<Integer> pagesAvailable {get; set;}
    public Integer availablePages{get;set;}
    public Integer targetPage{get;set;}
    public static final Integer ENTRIES = 50;

    public String ownerChoice{get;set;}
    public String managerChoice{get;set;}
    public SathersTrackerController() {
        RecordType rt = [SELECT id FROM RecordType WHERE DeveloperName = 'Customer_Group' AND SobjectType = 'Account'];
        baseQuery = 'SELECT id, Name, X2_for_1_50__c, X2_for_2__c, X2_for_3__c, X2_for_5__c, PAL__c, Carry_Sathers__c, Is_Exclusive__c, Carry_Competitors__c, Which_Competitor__c, Contract_Expiration_Date__c, Contract_Length_Years__c, Owner.Name, Owner.Manager.Name, OwnerId, Owner.ManagerId, Owner.UserRoleId, Owner.Manager.UserRoleId FROM Account WHERE RecordTypeId =\'' + rt.id +'\'';

        isAsc = true;
        orderBy = 'Name';
        sort();

        populateCurrentAccounts();
        initSelectOptions();


    }
    public void initSelectOptions(){
        ownerOptions = new List<SelectOption>();
        managerOptions = new List<SelectOption>();

        ownerOptions.add(new SelectOption('', 'All'));
        managerOptions.add(new SelectOption('', 'All'));

        Map<Id, String> ownerNamesByIds = new Map<Id, String>();
        Map<Id, String> managerNamesByIds = new Map<Id, String>();

        for(Account acc : allAccounts){
            if(!ownerNamesByIds.containsKey(acc.OwnerId)){
                ownerNamesByIds.put(acc.OwnerId, acc.Owner.Name);
            }
            if(acc.Owner.ManagerId != null && !managerNamesByIds.containsKey(acc.Owner.ManagerId)){
                managerNamesByIds.put(acc.Owner.ManagerId, acc.Owner.Manager.Name);
            }
        }
        for(Id ownerId : ownerNamesByIds.keySet()){
            ownerOptions.add(new SelectOption(ownerId, ownerNamesByIds.get(ownerId)));
        }
        for(Id manId : managerNamesByIds.keySet()){
            managerOptions.add(new SelectOption(manId, managerNamesByIds.get(manId)));
        }
    }
    public void filterByOwner(){
        if(String.isBlank(ownerChoice)){
            filterByManager();
            return;
        }

        String query = baseQuery;

        query += ' AND OwnerId = \'' + ownerChoice + '\'';
        query += getOrderBy();
        populateAccounts(query);
    }

    public void filterByManager(){
        if(String.isBlank(managerChoice)){
            ownerChoice = '';
            populateAccounts(baseQuery);
            return;
        }

        String query = baseQuery;

        query += ' AND Owner.ManagerId = \'' + managerChoice + '\'';
        query += getOrderBy();
        populateAccounts(query);
    }
    public void updateOwnerOptionList(){

        filterByManager();
        if(String.isBlank(managerChoice)){
            initSelectOptions();
            System.debug('manager choice is null');
            return;
        }
        ownerOptions.clear();
        ownerOptions.add(new SelectOption('', 'All'));

        User selectedManager = [SELECT id, (SELECT id, Name FROM ManagedUsers) FROM User WHERE id = :managerChoice];

        for(User u : selectedManager.ManagedUsers){
            ownerOptions.add(new SelectOption(u.id, u.Name));
        }
        System.debug(ownerOptions);
    }
    private void populateAccounts(String query){
        try{
            System.debug(query);
            allAccounts = (Account[]) Database.query(query);
            populateCurrentAccounts();
            currentPage = 1;
            calculateAvailablePages();
            populateListOfPages();
        }
        catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getMessage()));
            System.debug(e.getMessage());
            System.debug(e.getStackTraceString());
            return;
        }


    }
    public void sort(){
        if(orderBy != null){
            String query = baseQuery;
            if( previousOrderBy != null && orderBy == previousOrderBy){
                isAsc = !isAsc;
            }
            else{
                isAsc = true;
            }
            previousOrderBy = orderBy;

            if(String.isNotBlank(ownerChoice)){
                query += ' AND OwnerId = \'' + ownerChoice + '\'';
            } else if(String.isNotBlank(managerChoice)){
                query += ' AND Owner.ManagerId = \'' + managerChoice + '\'';
            }
            query += getOrderBy();
            populateAccounts(query);
        }
    }

    public String getOrderBy(){
        String orderByString;
        if(String.isNotBlank(orderBy)){
            orderByString = ' ORDER BY ' + orderBy;
        }
        else{
            return '';
        }
        if(isAsc){
            orderByString += ' ASC';
        }
        else{
            orderByString += ' DESC';
        }

        orderByString += ' NULLS LAST';


        return orderByString;
    }
    public PageReference save(){
        try{
            update allAccounts;
        }
        catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getMessage()));
            System.debug(e.getMessage());
            System.debug(e.getStackTraceString());
            return null;
        }
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Records Updated!'));
        return null;
    }
    public PageReference cancel(){
        return new PageReference('/');
    }
    public void populateCurrentAccounts(){
        currentAccounts = new List<Account>();
        for (Integer i = 0; i < ENTRIES;  i++) {
            if (i < allAccounts.size()) {
                currentAccounts.add(allAccounts.get(i));
            }
        }
    }
    public void calculateAvailablePages(){
        availablePages = allAccounts.size() / ENTRIES;
        if (Math.mod(allAccounts.size(),ENTRIES) > 0) {
            availablePages++;
        }

    }
    public void populateListOfPages(){
        pagesAvailable = new List<Integer>();
        for (Integer i = 0; i < availablePages; i++) {
            pagesAvailable.add(i);
        }
    }

    public PageReference nextPage() {
        if (currentPage + 1 <= pagesAvailable.size()) {
            currentAccounts.clear();
            currentPage++;
            for (Integer i = ENTRIES * currentPage - ENTRIES; i < ENTRIES * currentPage; i++) {
                if (i < allAccounts.size()) {
                    currentAccounts.add(allAccounts.get(i));
                }
            }
        }
        return null;
    }

    public PageReference previousPage() {
        if (currentPage - 1 > 0) {
            currentAccounts.clear();
            currentPage--;
            for (Integer i = ENTRIES * currentPage - ENTRIES; i < ENTRIES * currentPage; i++) {
                if (i < allAccounts.size()) {
                    currentAccounts.add(allAccounts.get(i));
                }
            }
        }
        return null;
    }

    public PageReference goToPage() {
        if (ApexPages.currentPage().getParameters().get('targetPage') != null) {
            targetPage = Integer.valueOf(ApexPages.currentPage().getParameters().get('targetPage'));
        }
        if (targetPage > 0 && targetPage <= pagesAvailable.size()) {
            currentAccounts.clear();
            currentPage = targetPage;
            for (Integer i = ENTRIES * currentPage - ENTRIES; i < ENTRIES * currentPage; i++) {
                if (i < allAccounts.size()) {
                    currentAccounts.add(allAccounts.get(i));
                }
            }
        }
        return null;
    }

    public void createRowsFromAccounts(AttachmentBodyWrapper bodyWrap, List<Account> accounts){
        if(accounts == null){
            return;
        }
        for(Account acct : accounts){
            bodyWrap.newRow(acct);
            bodyWrap.appendNewLine();
        }
    }

    public PageReference exportAsCSV(){

        if(allAccounts == null || allAccounts.size() == 0){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Nothing to export'));
            return null;
        }


        AttachmentBodyWrapper bodyWrap = new AttachmentBodyWrapper();


        createRowsFromAccounts(bodyWrap, allAccounts);
        if(bodyWrap.rows == 0){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'No rows to export'));
            return null;
        }
//        System.debug(bodyWrap.attachmentBody);
        Savepoint sp = Database.setSavepoint();
        try{
            CSV_Export__c csv_export = new CSV_Export__c();
            insert csv_export;
            Attachment att = new Attachment();
            att.Name = 'Sather`s Tracker Export:' + Date.today().format() + '.csv';
            att.Body = Blob.valueOf(bodyWrap.attachmentBody);
            att.ParentId = csv_export.id;
            att.ContentType = 'text/csv';
            insert att;

            return new PageReference('/servlet/servlet.FileDownload?file=' + att.id);
        }
        catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getMessage()));
            System.debug(e.getMessage());
            System.debug(e.getStackTraceString());
            Database.rollback(sp);
            return null;
        }
    }
//
    public class AttachmentBodyWrapper{
        final String CUSTOMER_HEADER = 'Customer';
        final String OWNER_HEADER= 'Owner';
        final String TEAM_LEAD_HEADER= 'Team Lead';
        List<String> headerValues;
        public String attachmentBody;
        public Integer rows;

        public AttachmentBodyWrapper(){
            headerValues = new List<String>{OWNER_HEADER, TEAM_LEAD_HEADER, 'Name', 'X2_for_1_50__c', 'X2_for_2__c', 'X2_for_3__c', 'X2_for_5__c', 'PAL__c', 'Carry_Sathers__c', 'Is_Exclusive__c', 'Carry_Competitors__c', 'Which_Competitor__c', 'Contract_Expiration_Date__c', 'Contract_Length_Years__c'};

            attachmentBody = '';
            attachmentBody += String.join(headerValues, ',');
            rows = 0;
            appendNewLine();
        }
        public void newRow(Account acct){
            if(acct == null){
                return;
            }
            for(String field : headerValues){
                if(field == OWNER_HEADER){
                    addValue(acct.Owner.Name);
                }
                else if(field == TEAM_LEAD_HEADER){

                    addValue(acct.Owner.Manager.Name);

                }
                else{
                    if(acct.get(field) != null){
                        attachmentBody += String.valueOf(acct.get(field)).escapeCsv();
                    }
                }
                attachmentBody += ',';
            }
            rows++;
        }
        public void addValue(String value){
            if(value != null){
                attachmentBody += value.escapeCsv();
            }
        }
//        public void newRow(Account acct){
//            if(allOther == null){
//                return;
//            }
//            for(String field : headerValues){
//                if(field == USER_HEADER){
//                    if(userName != null){
//                        addValue(userName);
//                    }
//                }
//                else if(field == ACCOUNT_HEADER){
//                    addValue('All Other');
//                }
//                else if(field == 'Replan__c'){
//                    if(allOther.plan != null){
//                        addValue(String.valueOf(allOther.plan));
//                    }
//                }
//                else if(field == 'LE_Sales_Call__c'){
//                    if(allOther.leCall != null){
//                        addValue(String.valueOf(allOther.leCall));
//
//                    }
//                }
//                else if(field == 'LE_Demand_Forecast__c'){
//                    if(allOther.leCall != null){
//                        addValue(String.valueOf(allOther.ledemand));
//
//                    }
//                }
//                else if(field == 'Latest_Estimate__c'){
//                    if(allOther.latestEstimate != null){
//                        addValue(String.valueOf(allOther.latestEstimate));
//                    }
//                }
//                else if(field == 'LE_Minus_Sales_Call__c'){
//                    if(allOther.leMinus != null){
//                        addValue(String.valueOf(allOther.leMinus));
//                    }
//                }
//                else if(field == 'Date__c'){
//                    if(allOther.salesCalls != null && allOther.salesCalls.size() > 0){
//                        //hack, asssuming all accts in All Other bucket have same date...
//                        Account acct = allOther.salesCalls.get(0);
//                        if(acct.Date__c != null){
//                            addValue(String.valueOf(acct.Date__c) + ' 0:00');
//                        }
//                    }
//                }
//                else if(field == 'S_O__c'){
//                    if(allOther.currentSO != null){
//                        addValue(String.valueOf(allOther.currentSO));
//                    }
//                }
//                attachmentBody += ',';
//            }
//        }
        public void appendNewLine(){
            attachmentBody += '\n';
        }
    }
}