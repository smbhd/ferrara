/**
 * Created by ChrisMatos on 12/21/2016.
 */

public class UserWrapper{
    public User theUser{get;set;}
    public List<UserWrapper> childrenWrappers{get;set;}
    public List<Sales_Call__c> salesCalls{get;set;}
    public AllOtherGroupWrapper allOtherGroup{get;set;}
    public Boolean expanded{get;set;}

    public UserWrapper(){
        expanded = false;
        childrenWrappers = new List<UserWrapper>();
        salesCalls = new List<Sales_Call__c>();
    }
    public void toggleChildren(){
        expanded = !expanded;
    }
    public void populateAllOther(){
        List<Sales_Call__c> allOther = new List<Sales_Call__c>();
        if(this.salesCalls != null){
            for(Integer i = 0; i < this.salesCalls.size(); i++){
                if(this.salesCalls.get(i).Customer__r.Is_All_Other__c == TRUE){
                    System.debug(this.salesCalls.get(i));
                    allOther.add(this.salesCalls.get(i));
                    this.salesCalls.remove(i);
                    i--;
                }
            }
        }
        if(allOther.size() > 0){
            this.allOtherGroup = new AllOtherGroupWrapper(allOther);
            System.debug(this.allOtherGroup);
        }
    }
    public class AllOtherGroupWrapper{
        public List<Sales_Call__c> salesCalls;
        public Decimal plan{get;set;}
        public Decimal leCall{get;set;}
        public Decimal currentSO{get;set;}
        public Decimal latestEstimate{get;set;}
        public Decimal leMinus{get;set;}
        public Decimal replanMinus{get;set;}
        public Decimal ledemand{get;set;}

        public AllOtherGroupWrapper(List<Sales_Call__c> calls){
            System.debug(calls);
            salesCalls = calls;
            plan = sumField('Replan__c');
            leCall = sumField('LE_Sales_Call__c');
            currentSO = sumField('S_O__c');
            latestEstimate = sumField('Latest_Estimate__c');
            leMinus = sumField('LE_Minus_Sales_Call__c');
            replanMinus = sumField('Replan_Minus_Sales_Call__C');
            ledemand = sumField('LE_Demand_Forecast__c');
        }
        public Decimal sumField(String fieldName){

            Decimal total = 0;
            for(Sales_Call__c sc : salesCalls){
                if(sc.get(fieldName) != null){
                    total += (Decimal)sc.get(fieldName);
                }
            }
            return total;

        }


    }
    public Decimal getChildTotalReplanMinusCall(){
        Decimal total = 0;
        total += this.getTotalReplanMinusCall();
        for(UserWrapper uw : childrenWrappers){
            total += uw.getChildTotalReplanMinusCall();
        }

        return total;
    }
    public Decimal getChildTotalLEMinusCall(){
        Decimal total = 0;
        total += this.getTotalLEMinusCall();
        for(UserWrapper uw : childrenWrappers){
            total += uw.getChildTotalLEMinusCall();
        }

        return total;
    }
    public Decimal getChildTotalPlan(){
        Decimal total = 0;
        total += this.getTotalPlan();
        for(UserWrapper uw : childrenWrappers){
            total += uw.getChildTotalPlan();
        }

        return total;
    }
    public Decimal getChildTotalLESalesCall(){
        Decimal total = 0;
        total += this.getTotalLESalesCall();
        for(UserWrapper uw : childrenWrappers){
            total += uw.getChildTotalLESalesCall();
        }
        return total;
    }
    public Decimal getChildTotalLEDemand(){
        Decimal total = 0;
        total += this.getTotalLEDemand();
        for(UserWrapper uw : childrenWrappers){
            total += uw.getChildTotalLEDemand();
        }
        return total;
    }
    public Decimal getChildTotalCurrentSO(){
        Decimal total = 0;
        total += this.getTotalCurrentSO();
        for(UserWrapper uw : childrenWrappers){
            total += uw.getChildTotalCurrentSO();
        }
        return total;
    }
    public Decimal getChildTotalLatestEstimate(){
        Decimal total = 0;
        total += this.getTotalLatestEstimate();
        for(UserWrapper uw : childrenWrappers){
            total += uw.getChildTotalLatestEstimate();
        }
        return total;
    }
    public Decimal getTotalLEMinusCall(){
        Decimal total = 0;
        if(salesCalls != null){
            for(Sales_Call__c sc : salesCalls){
                if(sc.LE_Minus_Sales_Call__c != null) {
                    total += sc.LE_Minus_Sales_Call__c;
                }
            }
        }
        if(allOtherGroup != null){
            total += allOtherGroup.leMinus;
        }
        return total;
    }
    public Decimal getTotalPlan(){
        Decimal total = 0;
        if(salesCalls != null){
            for(Sales_Call__c sc : salesCalls){
                if(sc.Replan__c != null) {
                    total += sc.Replan__c;
                }
            }
        }
        if(allOtherGroup != null){
            total += allOtherGroup.plan;
        }
        return total;
    }
    public Decimal getTotalLESalesCall(){
        Decimal total = 0;
        if(salesCalls != null) {
            for (Sales_Call__c sc : salesCalls) {
                if(sc.LE_Sales_Call__c != null) {
                    total += sc.LE_Sales_Call__c;
                }
            }
        }
        if(allOtherGroup != null){
            total += allOtherGroup.leCall;
        }
        return total;
    }
    public Decimal getTotalLEDemand(){
        Decimal total = 0;
        if(salesCalls != null) {
            for (Sales_Call__c sc : salesCalls) {
                if(sc.LE_Demand_Forecast__c != null) {
                    total += sc.LE_Demand_Forecast__c;
                }
            }
        }
        if(allOtherGroup != null){
            total += allOtherGroup.ledemand;
        }
        return total;
    }
    public Decimal getTotalCurrentSO(){
        Decimal total = 0;
        if(salesCalls != null){
            for(Sales_Call__c sc : salesCalls){
                if(sc.S_O__c != null) {
                    total += sc.S_O__c;
                }
            }
        }
        if(allOtherGroup != null){
            total += allOtherGroup.currentSO;
        }

        return total;
    }
    public Decimal getTotalLatestEstimate(){
        Decimal total = 0;
        if(salesCalls != null){
            for(Sales_Call__c sc : salesCalls){
                if(sc.Latest_Estimate__c != null){
                    total += sc.Latest_Estimate__c;
                }
            }
        }
        if(allOtherGroup != null){
            total += allOtherGroup.latestEstimate;
        }

        return total;
    }
    public Decimal getTotalReplanMinusCall(){
        Decimal total = 0;
        if(salesCalls != null){
            for(Sales_Call__c sc : salesCalls){
                if(sc.Replan_Minus_Sales_Call__c != null){
                    total += sc.Replan_Minus_Sales_Call__c;
                }
            }
        }
        if(allOtherGroup != null){
            total += allOtherGroup.replanMinus;
        }
        return total;
    }

}