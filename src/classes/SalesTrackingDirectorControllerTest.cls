/**
 * Created by AlexSanborn on 5/19/2017.
 */

@IsTest
public class SalesTrackingDirectorControllerTest {

    @TestSetup
    public static void setup(){
        User director = new User();
        director.Alias = 'DirectU';
        director.Email = 'DirectorUser@ferrara.com.smbdev';
        director.EmailEncodingKey = 'UTF-8';
        director.LastName = 'Director';
        director.LanguageLocaleKey = 'en_US';
        director.ProfileId = [SELECT id FROM Profile WHERE Name = 'Standard User'].id;
        director.UserRoleId = [SELECT id FROM UserRole WHERE DeveloperName = 'Sales_Director' LIMIT 1].id;
        director.TimeZoneSidKey = 'America/Chicago';
        director.Username = 'DirectorUser@ferrara.com.smbdev';
        director.LocaleSidKey = 'en_US';

        User director2 = new User();
        director2.Alias = 'ODirectU';
        director2.Email = 'OtherDirectorUser@ferrara.com.smbdev';
        director2.EmailEncodingKey = 'UTF-8';
        director2.LastName = 'Director2';
        director2.LanguageLocaleKey = 'en_US';
        director2.ProfileId = [SELECT id FROM Profile WHERE Name = 'Standard User'].id;
        director2.UserRoleId = [SELECT id FROM UserRole WHERE DeveloperName = 'Sales_Director' LIMIT 1].id;
        director2.TimeZoneSidKey = 'America/Chicago';
        director2.Username = 'OtherDirectorUser@ferrara.com.smbdev';
        director2.LocaleSidKey = 'en_US';

        User manager = new User();
        manager.Alias = 'manU';
        manager.Email = 'ManagerUser@ferrara.com.smbdev';
        manager.EmailEncodingKey = 'UTF-8';
        manager.LastName = 'Manager';
        manager.LanguageLocaleKey = 'en_US';
        manager.ProfileId = [SELECT id FROM Profile WHERE Name = 'Standard User'].id;
        manager.UserRoleId = [SELECT id FROM UserRole WHERE DeveloperName = 'Sales_Manager' LIMIT 1].id;
        manager.TimeZoneSidKey = 'America/Chicago';
        manager.Username = 'ManagerUser@ferrara.com.smbdev';
        manager.LocaleSidKey = 'en_US';

        User manager2 = new User();
        manager2.Alias = 'OManU';
        manager2.Email = 'OtherManagerUser@ferrara.com.smbdev';
        manager2.EmailEncodingKey = 'UTF-8';
        manager2.LastName = 'OtherManager';
        manager2.LanguageLocaleKey = 'en_US';
        manager2.ProfileId = [SELECT id FROM Profile WHERE Name = 'Standard User'].id;
        manager2.UserRoleId = [SELECT id FROM UserRole WHERE DeveloperName = 'Sales_Manager' LIMIT 1].id;
        manager2.TimeZoneSidKey = 'America/Chicago';
        manager2.Username = 'OtherManagerUser@ferrara.com.smbdev';
        manager2.LocaleSidKey = 'en_US';

        List<User> users = new List<User>{director, director2, manager, manager2};

        insert users;

        System.runAs(director){
            Account customer = new Account();
            customer.RecordTypeId = [SELECT id FROM RecordType WHERE SobjectType = 'Account' AND DeveloperName = 'Customer_Group'].id;
            customer.Name = 'Test Customer Group';
            customer.OwnerId = [SELECT id FROM User WHERE Username = 'ManagerUser@ferrara.com.smbdev'].id;

            Account customer2 = new Account();
            customer2.RecordTypeId = [SELECT id FROM RecordType WHERE SobjectType = 'Account' AND DeveloperName = 'Customer_Group'].id;
            customer2.Name = 'Test Customer Other Group';
            customer2.OwnerId = [SELECT id FROM User WHERE Username = 'OtherManagerUser@ferrara.com.smbdev'].id;

            List<Account> customers = new List<Account>{customer, customer2};
            insert customers;

            Sales_Call__c call = new Sales_Call__c();
            call.Plan__c = 100;
            call.LE_Sales_Call__c = 200;
            call.S_O__c = 100;
            call.Date__c = Date.today().toStartOfMonth();
            call.Customer__c = customer.id;

            Sales_Call__c call2 = new Sales_Call__c();
            call2.Plan__c = 100;
            call2.LE_Sales_Call__c = 200;
            call2.S_O__c = 100;
            call2.Date__c = call.Date__c.addMonths(1).toStartOfMonth();
            call2.Customer__c = customer2.id;

            Sales_Call__c call3 = new Sales_Call__c();
            call3.Plan__c = 100;
            call3.LE_Sales_Call__c = 200;
            call3.S_O__c = 100;
            call3.Latest_Estimate__c = 20;
            call3.Date__c = Date.today().toStartOfMonth();
            call3.Customer__c = customer2.id;

            List<Sales_Call__c> calls = new List<Sales_Call__c>{call, call2, call3};
            insert calls;
        }

    }

    @IsTest
    public static void itShouldLoadAllDataForOneDirector(){
        User director = [SELECT id, Name, ManagerId FROM User WHERE Username = 'DirectorUser@ferrara.com.smbdev'];
        System.runAs(director){
            Map<Id, Map<Integer, List<Sales_Call__c>>> callsByManager = SalesTrackingDirectorController.getSalesCallMapAllAccounts(Date.today().addMonths(-1), Date.today().addMonths(2));
            System.assertEquals(2, callsByManager.size());

            Integer callNum = 0;
            for(Id key : callsByManager.keySet()){
                Map<Integer, List<Sales_Call__c>> callSet = callsByManager.get(key);
                for(Integer innerKey : callSet.keySet()){
                    callNum += callSet.get(innerKey).size();
                }
            }
            System.assertEquals(3, callNum);
        }
    }

    @IsTest
    public static void itShouldLoadAllDataForOtherDirector(){
        User otherDirector = [SELECT id, Name, ManagerId FROM User WHERE Username = 'OtherDirectorUser@ferrara.com.smbdev'];
        System.runAs(otherDirector){
            Map<Id, Map<Integer, List<Sales_Call__c>>> callsByManager = SalesTrackingDirectorController.getSalesCallMapAllAccounts(Date.today().addMonths(-1), Date.today().addMonths(2));
            System.assertEquals(2, callsByManager.size());

            Integer callNum = 0;
            for(Id key : callsByManager.keySet()){
                Map<Integer, List<Sales_Call__c>> callSet = callsByManager.get(key);
                for(Integer innerKey : callSet.keySet()){
                    callNum += callSet.get(innerKey).size();
                }
            }
            System.assertEquals(3, callNum);
        }
    }
}