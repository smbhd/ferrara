/**
 * Created by ChrisMatos on 2/3/2017.
 */

public with sharing class DistributionController {

    public List<RetailerWrapper> retailerWrappers{get;set;}
    public List<RetailerWrapper> currentRetailerWrappers{get;set;}
    public Integer availablePages{get;set;}
    private List<Account> retailers;
    public static final Integer ENTRIES = 25;
    public Integer targetPage{get;set;}
    public Integer currentPage {get; set;}
    public List<Integer> pagesAvailable {get; set;}
    public String managerChoice{get;set;}
    public String brandChoice{get;set;}
    public String accountChoice{get;set;}
    public String teamLeadChoice{get;set;}
    public List<SelectOption> managerOptions{get;set;}
    public List<SelectOption> brandOptions{get;set;}
    public List<SelectOption> accountOptions{get;set;}
    public List<SelectOption> teamLeadOptions{get;set;}
    private String orderBy;
    private String baseQuery;
    private String baseSubQuery;
    private Id retailerRecTypeId;
    private String initialWhereClause;
    public DistributionController(){
        retailerRecTypeId = [SELECT id FROM RecordType WHERE SobjectType = 'Account' AND DeveloperName = 'Retailer'].id;
        currentPage = 1;
        targetPage = 1;
        managerChoice = '';
        brandChoice = '';
        accountChoice = '';
        teamLeadChoice = '';

        baseQuery = 'SELECT id, Name, RecordTypeId, Owner.Name, OwnerId,Owner.ManagerId, Owner.Manager.Name, (';
        baseSubQuery = 'SELECT id, Item__c, Item__r.Name,Item__r.Brand__c, Item__r.Item_Code__c, TDP__c, PY_TDP__c,Planned_TDP__c  FROM Opportunities';
        initialWhereClause = ') FROM Account WHERE RecordTypeId =\'' +retailerRecTypeId+ '\'';
        orderBy = ' ORDER BY Name ASC';
        filterRecords();

    }
    public void populateAllOptions(){
        managerOptions = new List<SelectOption>();
        teamLeadOptions = new List<SelectOption>();
        accountOptions = new List<SelectOption>();
        brandOptions = new List<SelectOption>();

        Set<Id> managerIds = new Set<Id>();
        Set<Id> accountIds = new Set<Id>();
        Set<Id> teamLeadIds = new Set<Id>();
        Set<String> brandSet = new Set<String>();
        for(Account acc : retailers){
            if(acc.OwnerId != null && !managerIds.contains(acc.OwnerId) && (acc.Opportunities != null && acc.Opportunities.size() > 0) ){
                managerIds.add(acc.OwnerId);
                managerOptions.add(new SelectOption(acc.OwnerId, acc.Owner.Name));
            }
            if(!accountIds.contains(acc.Id) && (acc.Opportunities != null && acc.Opportunities.size() > 0) ){
                accountIds.add(acc.Id);
                accountOptions.add(new SelectOption(acc.Id, acc.Name));
            }
            if(acc.Owner.ManagerId != null && !teamLeadIds.contains(acc.Owner.ManagerId) && (acc.Opportunities != null && acc.Opportunities.size() > 0) ){
                teamLeadIds.add(acc.Owner.ManagerId);
                teamLeadOptions.add(new SelectOption(acc.Owner.ManagerId, acc.Owner.Manager.Name));
            }
            for(Opportunity opp : acc.Opportunities){
                if(opp.Item__r.Brand__c != null && !brandSet.contains(opp.Item__r.Brand__c)){
                    brandSet.add(opp.Item__r.Brand__c);
                    brandOptions.add(new SelectOption(opp.Item__r.Brand__c, opp.Item__r.Brand__c));
                }
            }


        }
        if(managerOptions.size() == 0){
            managerOptions.add(new SelectOption('', '--None--'));
        }
        else{
            managerOptions = Utils.selectOptionSortByLabel(managerOptions);
            managerOptions.add(0,new SelectOption('', '--None--'));
        }
        if(teamLeadOptions.size() == 0){
            teamLeadOptions.add(new SelectOption('',  '--None--'));
        }
        else{
            teamLeadOptions = Utils.selectOptionSortByLabel(teamLeadOptions);
            teamLeadOptions.add(0,new SelectOption('',  '--None--'));
        }
        if(accountOptions.size() == 0){
            accountOptions.add(new SelectOption('', '--None--'));
        }
        else{
            accountOptions = Utils.selectOptionSortByLabel(accountOptions);
            accountOptions.add(0,new SelectOption('', '--None--'));
        }
        if(brandOptions.size() == 0){
            brandOptions.add(new SelectOption('', '--None--'));
        }
        else{
            brandOptions = Utils.selectOptionSortByLabel(brandOptions);
            brandOptions.add(0,new SelectOption('', '--None--'));
        }

    }


    public void filterRecords(){
        String query = buildQuery();
        try{
            System.debug(query);
            retailers = Database.query(query);
            populateRetailerWrappers();

            calculateAvailablePages();
            populateListOfPages();
            populateCurrentWrappers();

            populateAllOptions();
        }
        catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getMessage()));
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getStackTraceString()));
            System.debug(e.getMessage());
            System.debug(e.getStackTraceString());
        }

    }
    public String buildQuery(){
        String query = baseQuery;
        query += baseSubQuery;
        if(String.isNotBlank(brandChoice)){
            query += ' WHERE Item__r.Brand__c = \'' + String.escapeSingleQuotes(brandChoice)+ '\'';
        }
        query += initialWhereClause;
        query += getFilters();
        query += orderBy;
        return query;
    }
    public String getFilters(){
        String filter = '';
        if(String.isNotBlank(teamLeadChoice)){
            filter += ' AND Owner.ManagerId = \'' + String.escapeSingleQuotes(teamLeadChoice) + '\'';
        }
        else if(String.isNotBlank(managerChoice)){
            filter += ' AND OwnerId = \'' + String.escapeSingleQuotes(managerChoice) + '\'';
        }
        else if(String.isNotBlank(accountChoice)){
            filter += ' AND Id = \'' + String.escapeSingleQuotes(accountChoice) + '\'';
        }
        return filter;
    }
    public void populateRetailerWrappers(){
        retailerWrappers = new List<RetailerWrapper>();

        for(Account retailer : retailers){
            RetailerWrapper rw = new RetailerWrapper(retailer);
            Map<Id,List<Opportunity>> opportunitiesByProductId = new Map<Id,List<Opportunity>>();
            for(Opportunity opp : retailer.Opportunities){
                if(!opportunitiesByProductId.containsKey(opp.Item__c)){
                    opportunitiesByProductId.put(opp.Item__c, new List<Opportunity>());
                }
                opportunitiesByProductId.get(opp.Item__c).add(opp);
            }
            for(Id pId : opportunitiesByProductId.keySet()){
                if(opportunitiesByProductId.get(pId).size() > 0){
                    ProductWrapper pw = new ProductWrapper(pId);
                    pw.opportunities.addAll(opportunitiesByProductId.get(pId));
                    pw.upc = opportunitiesByProductId.get(pId).get(0).Item__r.Item_Code__c;
                    pw.description = opportunitiesByProductId.get(pId).get(0).Item__r.Name;
                    pw.brand = opportunitiesByProductId.get(pId).get(0).Item__r.Brand__c;
                    rw.productWrappers.add(pw);
                }

            }
            if(rw.productWrappers.size() > 0){
                retailerWrappers.add(rw);
            }
        }

    }
    public void populateCurrentWrappers(){
        currentRetailerWrappers = new List<RetailerWrapper>();
        for (Integer i = 0; i < ENTRIES;  i++) {
            if (i < retailerWrappers.size()) {
                currentRetailerWrappers.add(retailerWrappers.get(i));
            }
        }
    }
    public void calculateAvailablePages(){
        availablePages = retailerWrappers.size() /  ENTRIES;
        if (Math.mod(retailerWrappers.size(),ENTRIES) > 0) {
            availablePages++;
        }

    }

    public void populateListOfPages(){
        pagesAvailable = new List<Integer>();
        for (Integer i = 0; i < availablePages; i++) {
            pagesAvailable.add(i);
        }
    }

    public PageReference nextPage() {
        if (currentPage + 1 <= pagesAvailable.size()) {
            currentRetailerWrappers.clear();
            currentPage++;
            for (Integer i = ENTRIES * currentPage - ENTRIES; i < ENTRIES * currentPage; i++) {
                if (i < retailerWrappers.size()) {
                    currentRetailerWrappers.add(retailerWrappers.get(i));
                }
            }
        }
        return null;
    }

    public PageReference previousPage() {
        if (currentPage - 1 > 0) {
            currentRetailerWrappers.clear();
            currentPage--;
            for (Integer i = ENTRIES * currentPage - ENTRIES; i < ENTRIES * currentPage; i++) {
                if (i < retailerWrappers.size()) {
                    currentRetailerWrappers.add(retailerWrappers.get(i));
                }
            }
        }
        return null;
    }

    public PageReference goToPage() {
        if (ApexPages.currentPage().getParameters().get('targetPage') != null) {
            targetPage = Integer.valueOf(ApexPages.currentPage().getParameters().get('targetPage'));
        }
        if (targetPage > 0 && targetPage <= pagesAvailable.size()) {
            currentRetailerWrappers.clear();
            currentPage = targetPage;
            for (Integer i = ENTRIES * currentPage - ENTRIES; i < ENTRIES * currentPage; i++) {
                if (i < retailerWrappers.size()) {
                    currentRetailerWrappers.add(retailerWrappers.get(i));
                }
            }
        }
        return null;
    }

    public Double leTDPTotal{
        get{
            if(leTDPTotal == null){
                Double count = 0;
                for(RetailerWrapper rw : retailerWrappers){
                    if(rw.leTDPTotal != null){
                        count += rw.leTDPTotal;
                    }
                }
                return count;
            }
            else{
                return leTDPTotal;
            }
        }
    }
    public Double pyTotal{
        get{
            if(pyTotal == null){
                Double count = 0;
                for(RetailerWrapper rw : retailerWrappers){
                    if(rw.pyTotal != null){
                        count += rw.pyTotal;
                    }
                }
                return count;
            }
            else{
                return pyTotal;
            }
        }
    }

    public Double planTotal{
        get{
            if(planTotal == null){
                Double count = 0;
                for(RetailerWrapper rw : retailerWrappers){
                    if(rw.planTotal != null){
                        count += rw.planTotal;
                    }
                }
                return count;
            }
            else{
                return planTotal;
            }
        }
    }


    public class RetailerWrapper{
        public Account retailer{get;set;}
        public List<ProductWrapper> productWrappers{get;set;}
        public RetailerWrapper(Account acc){
            retailer = acc;
            productWrappers = new List<ProductWrapper>();
        }
        public Double leTDPTotal{
            get{
                if(leTDPTotal == null){
                    Double count = 0;
                    for(ProductWrapper pw : productWrappers){
                        if(pw.leTDP != null){
                            count += pw.leTDP;
                        }
                    }
                    return count;
                }
                else{
                    return leTDPTotal;
                }
            }
        }
        public Double pyTotal{
            get{
                if(pyTotal == null){
                    Double count = 0;
                    for(ProductWrapper pw : productWrappers){
                        if(pw.pyTDP != null){
                            count += pw.pyTDP;
                        }
                    }
                    return count;
                }
                else{
                    return pyTotal;
                }
            }
        }
        public Double planTotal{
            get{
                if(planTotal == null){
                    Double count = 0;
                    for(ProductWrapper pw : productWrappers){
                        if(pw.planTDP != null){
                            count += pw.planTDP;
                        }
                    }
                    return count;
                }
                else{
                    return planTotal;
                }
            }
        }

    }
    public class ProductWrapper{
        public String upc{get;set;}
        public String description{get;set;}
        public List<Opportunity> opportunities{get;set;}
        public String prodId{get;set;}
        public String brand{get;set;}
        public Double leTDP{
            get{
                if(leTDP == null){
                    Double total = 0;
                    for(Opportunity opp : opportunities){
                        if(opp.TDP__c != null){
                            total += opp.TDP__c;
                        }

                    }
                    return total;
                }
                return leTDP;
            }
        }
        public Double pyTDP{
            get{
                if(pyTDP == null){
                    Double total = 0;
                    for(Opportunity opp : opportunities){
                        if(opp.PY_TDP__c != null){
                            total += opp.PY_TDP__c;
                        }

                    }
                    return total;
                }
                return pyTDP;
            }
        }
        public Double planTDP{
            get{
                if(planTDP == null){
                    Double total = 0;
                    for(Opportunity opp : opportunities){
                        if(opp.Planned_TDP__c != null){
                            total += opp.Planned_TDP__c;
                        }

                    }
                    return total;
                }
                return planTDP;
            }
        }
        public ProductWrapper(Id prodId){
            opportunities = new List<Opportunity>();
            this.prodId = prodId;
        }

    }
}