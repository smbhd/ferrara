/**
 * Created by ChrisMatos on 6/1/2017.
 */

public with sharing class RetailerPlanController {
    ApexPages.StandardController stdController;
    public List<Opportunity> opportunities { get; set; }
    public Id accountId { get; set; }
    public Integer counter { get; set; }
    public Boolean isAsc { get; set; }
    public String orderBy { get; set; }
    public String previousOrderBy { get; set; }
    public String baseQuery { get; set; }
    public String query {get;set;}
    public List<OpportunityWrapper> addedOpportunities { get; set; }

    public RetailerPlanController(ApexPages.StandardController controller) {
        stdController = controller;

        accountId = stdController.getId();

        baseQuery = 'SELECT id, StageName, CloseDate, Item__r.Name, Item_Description__c, Base_Revenue__c, Plan_Revenue__c, Base_Distribution_Status__c, Base_Store_Count__c, Base_Reset_Date__c, Plan_Reset_Date__c, Plan_Distribution_Status__c, Plan_Store_Count__c, LE_v_PY_TDP__c, LE_v_Planned_TDP__c, TDP__c, PY_TDP__c, Planned_TDP__c, Item__r.Annual_Revenue__c FROM Opportunity WHERE AccountId =\'' + accountId + '\' AND RecordTypeId=\'' + retailerOppRecordType.id + '\'';

        query = baseQuery;
        opportunities = Database.query(query);
        wrapOpportunities();
    }

    public void wrapOpportunities() {
        opportunities = Database.query(query);
        List<OpportunityWrapper> newOpps = new List<OpportunityWrapper>();
        for (Integer i = 0; i < opportunities.size(); i++) {
            Opportunity current = opportunities.get(i);
            OpportunityWrapper newOpp = new OpportunityWrapper();
            newOpp.index = i;
            newOpp.opp = setDistributionStatuses(current);
            newOpp.amountPerStore = 0;
            newOpp.item = new Product2(id = current.Item__c, Name = current.Item__r.Name, Annual_Revenue__c = current.Item__r.Annual_Revenue__c);
            newOpps.add(newOpp);
            counter = newOpp.index;
        }
        addedOpportunities = newOpps;
    }

    public Opportunity setDistributionStatuses(Opportunity opp) {
        System.debug(opp);
        if (opp.Base_Distribution_Status__c == null) {
            if(opp.stageName == 'Pending'){
                opp.Base_Distribution_Status__c = null;
            } else if(opp.PY_TDP__c == 0){
                opp.Base_Distribution_Status__c = 'Gain';
                opp.Base_Reset_Date__c = opp.CloseDate;
            } else if(opp.PY_TDP__c > 0 && opp.TDP__c > 0){
                opp.Base_Distribution_Status__c = 'Current';
//                opp.Base_Reset_Date__c == fyDist;
            } else if(opp.PY_TDP__c > 0 && opp.TDP__c == 0){
                System.debug(opp);
                opp.Base_Distribution_Status__c = 'Loss';
                opp.Base_Reset_Date__c = opp.CloseDate;
            }
        }
        if (opp.Plan_Distribution_Status__c == null) {
            if (opp.Base_Distribution_Status__c == 'Loss') {
                opp.Plan_Distribution_Status__c = 'No Distribution';
                opp.Plan_Reset_Date__c = null;
            } else if (opp.Base_Distribution_Status__c == 'Gain' || opp.Base_Distribution_Status__c == 'Current') {
                opp.Plan_Distribution_Status__c = 'Current';
                //                opp.Plan_Reset_Date__c == fyDist;
            } else if (opp.Base_Distribution_Status__c == 'No Distribution'){
                opp.Plan_Distribution_Status__c = 'Gain';
            }
        }
        return opp;
    }

    public void addOpportunityLine() {
        try {
            Opportunity newOpp = new Opportunity();
            OpportunityWrapper newOppWrap = new OpportunityWrapper();
            if (counter == null) {
                counter = 0;
                newOppWrap.index = counter;
            } else {
                counter += 1;
                newOppWrap.index = counter;
            }
            newOppWrap.opp = newOpp;
            newOppWrap.amountPerStore = 0;
            addedOpportunities.add(newOppWrap);

        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getMessage()));
            System.debug(e.getMessage());
            System.debug(e.getStackTraceString());
            return;
        }
    }

    public void updateOppWithSKU() {
        try {
            String index = System.currentPageReference().getParameters().get('oppToUpdate');
            Integer indexNum = Integer.valueOf(index);

            for (OpportunityWrapper oppWrap : addedOpportunities) {
                if (oppWrap.index == indexNum) {
                    if(oppWrap.opp.Item__c == null){
                        throw new retailException('One or more of your Opportunities does not have a SKU. Please either add the SKU or delete the row from the table.');
                    }
                    Product2 product = [SELECT id, Name, Annual_Revenue__c FROM Product2 WHERE id = :oppWrap.opp.Item__c];

                    if(product.Annual_Revenue__c == null){
                        throw new retailException(product.Name + ' does not have an annual revenue $/Store amount. Please add it before trying to generate annual revenue data about the item.');
                    } else {
                        oppWrap.item = new Product2(id = product.id, Name = product.Name, Annual_Revenue__c = product.Annual_Revenue__c);
                    }
                }
            }
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getMessage()));
            System.debug(e.getMessage());
            System.debug(e.getStackTraceString());
            return;
        }
    }

    public void deleteOpportunity() {
        try {
            String index = System.currentPageReference().getParameters().get('index');
            Integer indexNum = Integer.valueOf(index);
            List<OpportunityWrapper> newOpps = new List<OpportunityWrapper>();

            for (OpportunityWrapper oppWrap : addedOpportunities) {
                if (oppWrap.index != indexNum) {
                    newOpps.add(oppWrap);
                } else {
                    if (oppWrap.opp.id != null) {
                        delete oppWrap.opp;
                    }
                }
            }
            addedOpportunities = newOpps;

        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getMessage()));
            System.debug(e.getMessage());
            System.debug(e.getStackTraceString());
            return;
        }
    }

    public void save() {
        try {
            List<Opportunity> oppsToAdd = new List<Opportunity>();
            List<Id> items = new List<id>();

            for (OpportunityWrapper oppWrap : addedOpportunities) {
                System.debug(oppWrap);

                if(oppWrap.item.Annual_Revenue__c == null){
                    throw new retailException('One or more of your items does not have an annual revenue $/Store amount. Please add it in order to generate annual revenue data about the item.');
                }
                if(String.isNotBlank(oppWrap.opp.Base_Distribution_Status__c)){
                    if(oppWrap.opp.Base_Distribution_Status__c == 'No Distribution' && oppWrap.opp.Plan_Distribution_Status__c == 'Gain'){
                        oppWrap.opp.StageName = 'Pending';
                    } else {
                        oppWrap.opp.StageName = 'Confirmed';
                    }
                }

                if(oppWrap.opp.Plan_Reset_Date__c != null){
                    oppWrap.opp.CloseDate = oppWrap.opp.Plan_Reset_Date__c;
                } else if(oppWrap.opp.Base_Reset_Date__c != null && oppWrap.opp.Plan_Distribution_Status__c == 'No Distribution'){
                    oppWrap.opp.CloseDate = oppWrap.opp.Base_Reset_Date__c;
                }

                if((String.isNotBlank(oppWrap.opp.Base_Distribution_Status__c) && oppWrap.opp.Base_Distribution_Status__c != 'No Distribution' && oppWrap.opp.TDP__c == null) || (String.isNotBlank(oppWrap.opp.Plan_Distribution_Status__c) && oppWrap.opp.Plan_Distribution_Status__c != 'No Distribution' && oppWrap.opp.Plan_Store_Count__c == null)){
                    throw new retailException('One or more of your Opportunities requires a store count.');
                }

                oppWrap.opp.AccountId = accountId;
                System.debug(oppWrap);
                oppWrap.opp.Name = oppWrap.item.Name;
                oppWrap.opp.RecordTypeId = retailerOppRecordType.id;
                System.debug(oppWrap);

                if(oppWrap.opp.Base_Revenue__c == null){
                    if(oppWrap.opp.Base_Distribution_Status__c != null){
                        if(oppWrap.opp.Base_Distribution_Status__c == 'Current' && oppWrap.opp.TDP__c != null && oppWrap.item.Annual_Revenue__c != null){
                            oppWrap.opp.Base_Revenue__c = calculateRevenue(oppWrap.item.Annual_Revenue__c, oppWrap.opp.TDP__c, oppWrap.opp.Base_Distribution_Status__c, 1);
                        } else if(oppWrap.opp.Base_Distribution_Status__c == 'No Distribution'){
                            oppWrap.opp.Base_Revenue__c = calculateRevenue(0, 0, oppWrap.opp.Base_Distribution_Status__c, 0);
                            oppWrap.opp.TDP__c = 0;
                            oppWrap.opp.Base_Reset_Date__c = null;
                        } else {
                            if(oppWrap.opp.Base_Reset_Date__c == null){
                                throw new retailException('One or more of your Opportunities with a status of Gain or Loss requires a reset date.');
                            }
                            Integer month = oppWrap.opp.Base_Reset_Date__c.month();
                            Decimal baseMonth = month * 1.0;
                            oppWrap.opp.Base_Revenue__c = calculateRevenue(oppWrap.item.Annual_Revenue__c, oppWrap.opp.TDP__c, oppWrap.opp.Base_Distribution_Status__c, baseMonth);
                        }
                    }
                }

                if(oppWrap.opp.Plan_Revenue__c == null){
                    if(oppWrap.opp.Plan_Distribution_Status__c != null){
                        if(oppWrap.opp.Plan_Distribution_Status__c == 'Current' && oppWrap.opp.Plan_Store_Count__c!= null && oppWrap.item.Annual_Revenue__c != null){
                            oppWrap.opp.Plan_Revenue__c = calculateRevenue(oppWrap.item.Annual_Revenue__c, oppWrap.opp.Plan_Store_Count__c, oppWrap.opp.Plan_Distribution_Status__c, 1);
                        } else if(oppWrap.opp.Plan_Distribution_Status__c == 'No Distribution'){
                            oppWrap.opp.Plan_Revenue__c = calculateRevenue(0, 0, oppWrap.opp.Plan_Distribution_Status__c, 0);
                            oppWrap.opp.Plan_Store_Count__c = 0;
                            oppWrap.opp.Plan_Reset_Date__c = null;
                        } else {
                            if(oppWrap.opp.Plan_Reset_Date__c == null){
                                throw new retailException('One or more of your Opportunities with a status of Gain or Loss requires a reset date.');
                            }
                            Integer month = oppWrap.opp.Plan_Reset_Date__c.month();
                            Decimal baseMonth = month * 1.0;
                            oppWrap.opp.Plan_Revenue__c = calculateRevenue(oppWrap.item.Annual_Revenue__c, oppWrap.opp.Plan_Store_Count__c, oppWrap.opp.Plan_Distribution_Status__c, baseMonth);
                        }
                    }
                }


                oppsToAdd.add(oppWrap.opp);
            }
            upsert oppsToAdd;
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getMessage()));
            System.debug(e.getMessage());
            System.debug(e.getStackTraceString());
            return;
        }
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Records Updated'));
        return;
    }

    public Decimal calculateRevenue(Decimal annualRevenue, Decimal storeCount, String status, Decimal month) {
        Decimal FYRevenue = annualRevenue * storeCount;
        Decimal MonthlyRevenue = FYRevenue / 12;
        Decimal total;
        if (status == 'Current') {
            total = FYRevenue;
        } else if (status == 'Gain') {
            total = (FYRevenue * ((13 - month) / 12)) + (2 * MonthlyRevenue);
        } else if (status == 'Loss') {
            total = FYRevenue * (month / 12);
        } else {
            total = 0;
        }
        System.debug(total);
        return total;
    }

    public void updateRevenueValues() {
        try {
            String index = System.currentPageReference().getParameters().get('oppToUpdate');
            String event = System.currentPageReference().getParameters().get('event');
            Integer indexNum = Integer.valueOf(index);

            for (OpportunityWrapper oppWrap : addedOpportunities) {
                if (oppWrap.index == indexNum) {
                    if (oppWrap.opp.Base_Distribution_Status__c != null || oppWrap.opp.Plan_Distribution_Status__c != null) {
                        if (oppWrap.opp.Base_Distribution_Status__c == 'No Distribution') {
                            oppWrap.opp.Base_Revenue__c = calculateRevenue(0, 0, oppWrap.opp.Base_Distribution_Status__c, 0);
                            oppWrap.opp.Base_Distribution_Status__c = 'No Distribution';
                            oppWrap.opp.TDP__c = 0;
                            oppWrap.opp.Base_Reset_Date__c = null;
                            if(event == 'base'){
                                oppWrap.opp.Plan_Distribution_Status__c = 'Gain';
                            }
                        } else if (oppWrap.opp.Base_Distribution_Status__c == 'Current' && oppWrap.opp.TDP__c != null) {
                            oppWrap.opp.Base_Revenue__c = calculateRevenue(oppWrap.item.Annual_Revenue__c, oppWrap.opp.TDP__c, oppWrap.opp.Base_Distribution_Status__c, 1);
                            if (event == 'base') {
                                oppWrap.opp.Plan_Distribution_Status__c = 'Current';
                            }
                        } else {
                            if (oppWrap.opp.Base_Distribution_Status__c == 'Gain' && event == 'base') {
                                oppWrap.opp.Plan_Distribution_Status__c = 'Current';
                            } else if (oppWrap.opp.Base_Distribution_Status__c == 'Loss' && event == 'base') {
                                oppWrap.opp.Plan_Distribution_Status__c = 'No Distribution';
                                oppWrap.opp.Plan_Store_Count__c = 0;
                                oppWrap.opp.Plan_Reset_Date__c = null;
                            }

                            if (oppWrap.opp.Base_Reset_Date__c != null && oppWrap.opp.TDP__c != null) {
                                Integer month = oppWrap.opp.Base_Reset_Date__c.month();
                                Decimal baseMonth = month * 1.0;
                                oppWrap.opp.Base_Revenue__c = calculateRevenue(oppWrap.item.Annual_Revenue__c, oppWrap.opp.TDP__c, oppWrap.opp.Base_Distribution_Status__c, baseMonth);
                            }
                        }

                        if (oppWrap.opp.Plan_Distribution_Status__c == 'No Distribution') {
                            oppWrap.opp.Plan_Revenue__c = calculateRevenue(0, 0, oppWrap.opp.Plan_Distribution_Status__c, 0);
                        } else if (oppWrap.opp.Plan_Distribution_Status__c == 'Current' && oppWrap.opp.Plan_Store_Count__c != null) {
                            oppWrap.opp.Plan_Revenue__c = calculateRevenue(oppWrap.item.Annual_Revenue__c, oppWrap.opp.Plan_Store_Count__c, oppWrap.opp.Plan_Distribution_Status__c, 1);
                        } else if (oppWrap.opp.Plan_Reset_Date__c != null && oppWrap.opp.Plan_Store_Count__c != null) {
                            Integer month = oppWrap.opp.Plan_Reset_Date__c.month();
                            Decimal planMonth = month * 1.0;
                            oppWrap.opp.Plan_Revenue__c = calculateRevenue(oppWrap.item.Annual_Revenue__c, oppWrap.opp.Plan_Store_Count__c, oppWrap.opp.Plan_Distribution_Status__c, planMonth);
                        }
                    }
                }
            }
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getMessage()));
            System.debug(e.getMessage());
            System.debug(e.getStackTraceString());
            return;
        }
        return;

    }

    public void sort() {
        try {
            if (orderBy != null) {
                System.debug(orderBy);
                String orderByString = ' ORDER BY ' + orderBy;

                if (previousOrderBy == orderBy) {
                    if (isAsc) {
                        isAsc = false;
                        orderByString += ' DESC';

                    } else {
                        orderByString += ' ASC';
                        isAsc = true;
                    }
                } else {
                    orderByString += ' ASC';
                    isAsc = true;
                }
                orderByString += ' NULLS LAST';
                previousOrderBy = orderBy;
                query = baseQuery + orderByString;
                System.debug(isAsc);
                opportunities = Database.query(query);
                wrapOpportunities();
            }

        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getMessage()));
            System.debug(e.getMessage());
            System.debug(e.getStackTraceString());
            return;
        }
    }


    public void createRowsFromOpportunityWrappers(AttachmentBodyWrapper bodyWrap, List<OpportunityWrapper> oppWrappers) {
        if (oppWrappers == null) {
            return;
        }
        for (OpportunityWrapper ow : oppWrappers) {
            if (ow.opp != null) {
                bodyWrap.newRow(ow.opp);
                bodyWrap.appendNewLine();
            }
        }
    }

    public PageReference exportAsCSV() {

        if (addedOpportunities == null || addedOpportunities.size() == 0) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Nothing to export'));
            return null;
        }
        AttachmentBodyWrapper bodyWrap = new AttachmentBodyWrapper();
        wrapOpportunities();

        createRowsFromOpportunityWrappers(bodyWrap, addedOpportunities);
        if (bodyWrap.rows == 0) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'No rows to export'));
            return null;
        }
        System.debug(bodyWrap.attachmentBody);
        Savepoint sp = Database.setSavepoint();
        try {
            CSV_Export__c csv_export = new CSV_Export__c();
            insert csv_export;
            Attachment att = new Attachment();
            att.Name = 'Retailer ' + Date.today().year() + ' Planning.csv';
            att.Body = Blob.valueOf(bodyWrap.attachmentBody);
            att.ParentId = csv_export.id;
            att.ContentType = 'text/csv';
            insert att;
//
            return new PageReference('/servlet/servlet.FileDownload?file=' + att.id);
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getMessage()));
            System.debug(e.getMessage());
            System.debug(e.getStackTraceString());
            Database.rollback(sp);
            return null;
        }
    }
//
    public class AttachmentBodyWrapper {
        List<String> header1Values;
        List<String> header2Values;
        public String attachmentBody;
        public Integer rows;
//
        public AttachmentBodyWrapper() {
            header1Values = new List<String>{
                    'Regular Volume', '', '', '', '', '', '', '', Date.today().addYears(1).year() + ' $', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', Date.today().year() + ' $'
            };
            header2Values = new List<String>{'', 'Material #', 'Annual $/Store', Date.today().addYears(1).year() + ' Store Ct', 'Plan Reset Timing', 'Monthly $ ' + Date.today().addYears(1).year(), 'Pipeline ' + Date.today().addYears(1).year(), '', 'Jan ' + Date.today().addYears(1).year(), 'Feb ' + Date.today().addYears(1).year(), 'Mar ' + Date.today().addYears(1).year(), 'Apr ' + Date.today().addYears(1).year(), 'May ' + Date.today().addYears(1).year(), 'Jun ' + Date.today().addYears(1).year(), 'Jul ' + Date.today().addYears(1).year(), 'Aug ' + Date.today().addYears(1).year(), 'Sep ' + Date.today().addYears(1).year(), 'Oct ' + Date.today().addYears(1).year(), 'Nov ' + Date.today().addYears(1).year(), 'Dec ' + Date.today().addYears(1).year(), Date.today().addYears(1).year() + ' Total', '', 'Material #', Date.today().year() + ' Distribution', 'Annual $/Store', Date.today().year() + ' Store Ct', 'Base Reset Timing', 'Monthly $ ' + Date.today().year(), 'Pipeline ' + Date.today().year(), '', 'Jan ' + Date.today().year(), 'Feb ' + Date.today().year(), 'Mar ' + Date.today().year(), 'Apr ' + Date.today().year(), 'May ' + Date.today().year(), 'Jun ' + Date.today().year(), 'Jul ' + Date.today().year(), 'Aug ' + Date.today().year(), 'Sep ' + Date.today().year(), 'Oct ' + Date.today().year(), 'Nov ' + Date.today().year(), 'Dec ' + Date.today().year(), Date.today().year() + ' Total'};
            attachmentBody = '';
            attachmentBody += String.join(header1Values, ',');
            rows = 0;
            appendNewLine();
            attachmentBody += String.join(header2Values, ',');
            rows++;
            appendNewLine();
        }
        public void newRow(Opportunity opp) {
            if (opp == null) {
                return;
            }

            Map<String, String> baseMonthlyValues;
            Map<String, String> planMonthlyValues;
            Decimal baseMonth;
            Decimal planMonth;

            if(opp.Base_Distribution_Status__c != null){
                if(opp.Base_Distribution_Status__c == 'No Distribution'){
                    baseMonth = 0;
                } else if(opp.Base_Distribution_Status__c == 'Current'){
                    baseMonth = 1;
                } else if(opp.Base_Reset_Date__c != null){
                    Integer month = opp.Base_Reset_Date__c.month();
                    baseMonth = month * 1.0;
                }
                if(opp.Item__r.Annual_Revenue__c != null && opp.TDP__c != null){
                    baseMonthlyValues = getMonthlyValues(opp.Item__r.Annual_Revenue__c, opp.TDP__c, opp.Base_Distribution_Status__c, baseMonth);
                }
            }

            if(opp.Plan_Distribution_Status__c != null){
                if(opp.Plan_Distribution_Status__c == 'No Distribution'){
                    planMonth = 0;
                } else if(opp.Plan_Distribution_Status__c == 'Current'){
                    planMonth = 1;
                } else if(opp.Plan_Reset_Date__c != null){
                    Integer month = opp.Plan_Reset_Date__c.month();
                    planMonth = month * 1.0;
                }
                if(opp.Item__r.Annual_Revenue__c != null && opp.Plan_Store_Count__c != null){
                    planMonthlyValues = getMonthlyValues(opp.Item__r.Annual_Revenue__c, opp.Plan_Store_Count__c, opp.Plan_Distribution_Status__c, planMonth);
                }
            }

            for (String field : header2Values) {
                if(field == 'Material #'){
                    if(opp.Item__r.Name != null) {
                        addValue(opp.Item__r.Name);
                    }
                } else if (field == 'Annual $/Store') {
                    if (opp.Item__r.Annual_Revenue__c != null) {
                        addValue(String.valueOf(opp.Item__r.Annual_Revenue__c));
                    }
                } else if (field == Date.today().addYears(1).year() + ' Store Ct') {
                    if (opp.Plan_Store_Count__c != null) {
                        addValue(String.valueOf(opp.Plan_Store_Count__c));
                    }
                } else if (field == Date.today().year() + ' Store Ct') {
                    if (opp.TDP__c != null) {
                        addValue(String.valueOf(opp.TDP__c));
                    }
                } else if (field == 'Plan Reset Timing') {
                    if (opp.Plan_Reset_Date__c != null) {
                        addvalue(String.valueOf(opp.Plan_Reset_Date__c));
                    }
                } else if (field == 'Base Reset Timing') {
                    if (opp.Base_Reset_Date__c != null) {
                        addvalue(String.valueOf(opp.Base_Reset_Date__c));
                    }
                } else if (field == 'Monthly $ ' + Date.today().year()) {
                    if(baseMonthlyValues != null){
                        addValue(baseMonthlyValues.get('monthly'));
                    }
                } else if (field == 'Jan ' + Date.today().year()) {
                    if(baseMonthlyValues != null){
                        addValue(baseMonthlyValues.get('Jan'));
                    }
                } else if (field == 'Feb ' + Date.today().year()) {
                    if(baseMonthlyValues != null){
                        addValue(baseMonthlyValues.get('Feb'));

                    }
                }else if (field == 'Mar ' + Date.today().year()) {
                    if(baseMonthlyValues != null){
                        addValue(baseMonthlyValues.get('Mar'));

                    }
                }else if (field == 'Apr ' + Date.today().year()) {
                    if(baseMonthlyValues != null){
                        addValue(baseMonthlyValues.get('Apr'));

                    }
                }else if (field == 'May ' + Date.today().year()) {
                    if(baseMonthlyValues != null){
                        addValue(baseMonthlyValues.get('May'));

                    }
                }else if (field == 'Jun ' + Date.today().year()) {
                    if(baseMonthlyValues != null){
                        addValue(baseMonthlyValues.get('Jun'));

                    }
                }else if (field == 'Jul ' + Date.today().year()) {
                    if(baseMonthlyValues != null){
                        addValue(baseMonthlyValues.get('Jul'));

                    }
                }else if (field == 'Aug ' + Date.today().year()) {
                    if(baseMonthlyValues != null){
                        addValue(baseMonthlyValues.get('Aug'));

                    }
                }else if (field == 'Sep ' + Date.today().year()) {
                    if(baseMonthlyValues != null){
                        addValue(baseMonthlyValues.get('Sep'));

                    }
                }else if (field == 'Oct ' + Date.today().year()) {
                    if(baseMonthlyValues != null){
                        addValue(baseMonthlyValues.get('Oct'));

                    }
                }else if (field == 'Nov ' + Date.today().year()) {
                    if(baseMonthlyValues != null){
                        addValue(baseMonthlyValues.get('Nov'));

                    }
                }else if (field == 'Dec ' + Date.today().year()) {
                    if(baseMonthlyValues != null){
                        addValue(baseMonthlyValues.get('Dec'));

                    }
                } else if (field == 'Pipeline ' + Date.today().year()) {
                    if(opp.Base_Distribution_Status__c == 'Gain' && baseMonthlyValues != null){
                        addValue(baseMonthlyValues.get('pipeline'));
                    }
                } else if (field == 'Monthly $ ' + Date.today().addYears(1).year()) {
                    if(planMonthlyValues != null){
                        addValue(planMonthlyValues.get('monthly'));
                    }
                } else if (field == 'Jan ' + Date.today().addYears(1).year()) {
                    if(planMonthlyValues != null){
                        addValue(planMonthlyValues.get('Jan'));

                    }
                } else if (field == 'Feb ' + Date.today().addYears(1).year()) {
                    if(planMonthlyValues != null){
                        addValue(planMonthlyValues.get('Feb'));

                    }
                }else if (field == 'Mar ' + Date.today().addYears(1).year()) {
                    if(planMonthlyValues != null){
                        addValue(planMonthlyValues.get('Mar'));

                    }
                }else if (field == 'Apr ' + Date.today().addYears(1).year()) {
                    if(planMonthlyValues != null){
                        addValue(planMonthlyValues.get('Apr'));

                    }
                }else if (field == 'May ' + Date.today().addYears(1).year()) {
                    if(planMonthlyValues != null){
                        addValue(planMonthlyValues.get('May'));

                    }
                }else if (field == 'Jun ' + Date.today().addYears(1).year()) {
                    if(planMonthlyValues != null){
                        addValue(planMonthlyValues.get('Jun'));

                    }
                }else if (field == 'Jul ' + Date.today().addYears(1).year()) {
                    if(planMonthlyValues != null){
                        addValue(planMonthlyValues.get('Jul'));

                    }
                }else if (field == 'Aug ' + Date.today().addYears(1).year()) {
                    if(planMonthlyValues != null){
                        addValue(planMonthlyValues.get('Aug'));

                    }
                }else if (field == 'Sep ' + Date.today().addYears(1).year()) {
                    if(planMonthlyValues != null){
                        addValue(planMonthlyValues.get('Sep'));

                    }
                }else if (field == 'Oct ' + Date.today().addYears(1).year()) {
                    if(planMonthlyValues != null){
                        addValue(planMonthlyValues.get('Oct'));

                    }
                }else if (field == 'Nov ' + Date.today().addYears(1).year()) {
                    if(planMonthlyValues != null){
                        addValue(planMonthlyValues.get('Nov'));

                    }
                }else if (field == 'Dec ' + Date.today().addYears(1).year()) {
                    if(planMonthlyValues != null){
                        addValue(planMonthlyValues.get('Dec'));

                    }
                } else if (field == 'Pipeline ' + Date.today().addYears(1).year()) {
                    if(opp.Plan_Distribution_Status__c == 'Gain' && planMonthlyValues != null){
                        addValue(planMonthlyValues.get('pipeline'));
                    }
                } else if (field == Date.today().addYears(1).year() + ' Total') {
                    if(opp.Plan_Revenue__c != null){
                        addValue(String.valueOf(opp.Plan_Revenue__c));
                    }

                } else if (field == Date.today().year() + ' Total') {
                    if(opp.Base_Revenue__c != null){
                        addValue(String.valueOf(opp.Base_Revenue__c));
                    }
                }
                attachmentBody += ',';
            }
            rows++;
        }

        public Map<String, String> getMonthlyValues(Decimal annualRevenue, Decimal storeCount, String status, Decimal month){
            Map<String, String> monthlyValues = new Map<String, String>();
            List<String> months = new List<String>{'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'};

            Decimal FYRevenue = annualRevenue * storeCount;

            String monthlyVal = String.valueOf(FYRevenue/12);
            String pipeline = String.valueOf((FYRevenue/12)*2);

            if(status == 'Current'){
                for(String m : months){
                    monthlyValues.put(m, monthlyVal);
                }
                monthlyValues.put('monthly', monthlyVal);
            } else if (status == 'Gain'){
                for(Integer i=0; i < months.size(); i++){
                    Integer monthNum = i + 1;
                    if((month-1) == monthNum){
                        monthlyValues.put(months[i],pipeline);
                        monthlyValues.put('pipeline', pipeline);
                    } else if(month <= monthNum){
                        monthlyValues.put(months[i],monthlyVal);
                    } else {
                        monthlyValues.put(months[i], '0');
                    }
                }
                monthlyValues.put('monthly', monthlyVal);
            } else if(status == 'Loss'){
                for(Integer i=0; i < months.size(); i++){
                    Integer monthNum = i + 1;
                    if(month >= monthNum){
                        monthlyValues.put(months[i], monthlyVal);
                    } else {
                        monthlyValues.put(months[i], '0');
                    }
                }
                monthlyValues.put('monthly', monthlyVal);
            } else {
                for(String m : months){
                    monthlyValues.put(m, '0');
                }
                monthlyValues.put('monthly', '0');
            }
            System.debug(monthlyValues);
            return monthlyValues;
        }

        public void addValue(String value) {
            if (value != null) {
                attachmentBody += value.escapeCsv();
            }
        }

        public void appendNewLine() {
            attachmentBody += '\n';
        }
    }

    public String thisYear{
        get{
            return String.valueOf(Date.today().year());

        } private set;
    }

    public String nextYear{
        get{
            return String.valueOf(Date.today().addYears(1).year());
        } private set;
    }

    public RecordType retailerOppRecordType {
        get{
            if(retailerOppRecordType == null){
                retailerOppRecordType = [SELECT id FROM RecordType WHERE SobjectType='Opportunity' AND DeveloperName='Retailer_Opportunity'];
            }
            return retailerOppRecordType;
        } private set;
    }

    public class OpportunityWrapper{
        public Integer index{get;set;}
        public Opportunity opp{get;set;}
        public Double amountPerStore{get;set;}
        public Product2 item{get;set;}
    }

    public class retailException extends Exception {}
}

