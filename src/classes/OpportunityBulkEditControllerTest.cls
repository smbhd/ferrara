/**
 * Created by ChrisMatos on 11/3/2016.
 */
@isTest
public with sharing class OpportunityBulkEditControllerTest {
    @TestSetup
    public static void testSetup(){
        Account acc = new Account();
        acc.Name = 'Test Account';
        acc.RecordTypeId = [SELECT id FROM RecordType WHERE DeveloperName = 'Customer_Group'].id;
        insert acc;

        Opportunity opp = new Opportunity();
        opp.RecordTypeId = [SELECT id FROM RecordType WHERE DeveloperName = 'Opportunity'].id;
        opp.AccountId = acc.id;
        opp.Name = 'Test Opp';
        opp.Amount = 1000;
        opp.CloseDate = Date.today().addMonths(1);
        opp.StageName = 'Pending';
        insert opp;

        opp.id = null;
        opp.Name = 'Test Risk';
        opp.RecordTypeId = [SELECT id FROM RecordType WHERE DeveloperName = 'Risk'].id;
        opp.Amount = -1000;
        opp.CloseDate = Date.today();
        insert opp;

    }
    @IsTest
    public static void itShouldFilterOnLetterAndView(){

        OpportunityBulkEditController controller = new OpportunityBulkEditController();
        controller.viewChoice = 'All Opportunities';

        controller.filterByView();
        System.assertEquals(1, controller.currentOpps.size());
        controller.orderBy = 'Account';
        controller.sortHeader();
        controller.sortHeader();
        controller.filterLetter = 'T';
        controller.filterRecords();

        System.assertEquals(1, controller.currentOpps.size());

        controller.filterLetter = 'A';
        controller.filterRecords();
        System.assertEquals(0, controller.currentOpps.size());

        controller.viewChoice = 'All Risks';
        controller.filterByView();

        System.assertEquals(1, controller.currentOpps.size());

        controller.goToNewOpp();
        controller.goToNewRisk();


    }
    @IsTest
    public static void itShouldSupportMultiplePages(){
        Account acc = [SELECT id, Name FROM Account WHERE Name = 'Test Account'];
        List<Opportunity> opps = new List<Opportunity>();
        for(Integer i = 0; i < OpportunityBulkEditController.ENTRIES + 1; i++){
            Opportunity opp = new Opportunity();
            opp.AccountId = acc.id;
            opp.Name = 'Test Number' + i;
            opp.Amount = i;
            opp.CloseDate = Date.today();
            opp.StageName ='Pending';
            opps.add(opp);
        }
        insert opps;
        OpportunityBulkEditController controller = new OpportunityBulkEditController();
        System.assertEquals(2, controller.availablePages);
        controller.nextPage();
        System.assertEquals(2, controller.currentPage);
        controller.previousPage();
        System.assertEquals(1, controller.currentPage);
        controller.targetPage = 2;
        controller.goToPage();
        System.assertEquals(2, controller.currentPage);

    }
    @IsTest
    public static void itShouldSaveRecords(){
        Integer newAmount = 200;
        OpportunityBulkEditController controller = new OpportunityBulkEditController();
        Opportunity opp = controller.currentOpps.get(0);
        opp.Amount = newAmount;

        controller.save();

        opp = [SELECT id, Amount FROM Opportunity WHERE id = :opp.id];
        System.assertEquals(newAmount, opp.Amount);

    }
}