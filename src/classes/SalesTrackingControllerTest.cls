/**
 * Created by ChrisMatos on 11/10/2016.
 */

@isTest
public with sharing class SalesTrackingControllerTest {

    @TestSetup
    public static void setup(){

        User admin = new User();
        admin.Alias = 'AdminU';
        admin.Email = 'AdminUser@ferrara.com.smbdev';
        admin.EmailEncodingKey = 'UTF-8';
        admin.LastName = 'Admin';
        admin.LanguageLocaleKey = 'en_US';
        admin.ProfileId = [SELECT id FROM Profile WHERE Name = 'System Administrator' LIMIT 1].id;
        admin.TimeZoneSidKey = 'America/Chicago';
        admin.Username = 'AdminUser@ferrara.com.smbdev';
        admin.LocaleSidKey = 'en_US';


        User director = new User();
        director.Alias = 'DirectU';
        director.Email = 'DirectorUser@ferrara.com.smbdev';
        director.EmailEncodingKey = 'UTF-8';
        director.LastName = 'Director';
        director.LanguageLocaleKey = 'en_US';
        director.ProfileId = [SELECT id FROM Profile WHERE Name = 'Standard User'].id;
        director.UserRoleId = [SELECT id FROM UserRole WHERE DeveloperName = 'Sales_Director' LIMIT 1].id;
        director.TimeZoneSidKey = 'America/Chicago';
        director.Username = 'DirectorUser@ferrara.com.smbdev';
        director.LocaleSidKey = 'en_US';

        User director2 = new User();
        director2.Alias = 'ODirectU';
        director2.Email = 'OtherDirectorUser@ferrara.com.smbdev';
        director2.EmailEncodingKey = 'UTF-8';
        director2.LastName = 'Director2';
        director2.LanguageLocaleKey = 'en_US';
        director2.ProfileId = [SELECT id FROM Profile WHERE Name = 'Standard User'].id;
        director2.UserRoleId = [SELECT id FROM UserRole WHERE DeveloperName = 'Regional_East_Director' LIMIT 1].id;
        director2.TimeZoneSidKey = 'America/Chicago';
        director2.Username = 'OtherDirectorUser@ferrara.com.smbdev';
        director2.LocaleSidKey = 'en_US';

        User manager = new User();
        manager.Alias = 'manU';
        manager.Email = 'ManagerUser@ferrara.com.smbdev';
        manager.EmailEncodingKey = 'UTF-8';
        manager.LastName = 'Manager';
        manager.LanguageLocaleKey = 'en_US';
        manager.ProfileId = [SELECT id FROM Profile WHERE Name = 'Standard User'].id;
        manager.UserRoleId = [SELECT id FROM UserRole WHERE DeveloperName = 'Sales_Manager' LIMIT 1].id;
        manager.TimeZoneSidKey = 'America/Chicago';
        manager.Username = 'ManagerUser@ferrara.com.smbdev';
        manager.LocaleSidKey = 'en_US';

        User manager2 = new User();
        manager2.Alias = 'OManU';
        manager2.Email = 'OtherManagerUser@ferrara.com.smbdev';
        manager2.EmailEncodingKey = 'UTF-8';
        manager2.LastName = 'OtherManager';
        manager2.LanguageLocaleKey = 'en_US';
        manager2.ProfileId = [SELECT id FROM Profile WHERE Name = 'Standard User'].id;
        manager2.UserRoleId = [SELECT id FROM UserRole WHERE DeveloperName = 'Sales_Manager' LIMIT 1].id;
        manager2.TimeZoneSidKey = 'America/Chicago';
        manager2.Username = 'OtherManagerUser@ferrara.com.smbdev';
        manager2.LocaleSidKey = 'en_US';

        List<User> users = new List<User>{admin, director, director2, manager, manager2};

        insert users;

        manager.ManagerId = director.id;
        manager2.ManagerId = director2.id;

        List<User> managers = new List<User>{manager, manager2};

        update managers;

        User pillar = [SELECT id, Name FROM User WHERE UserRole.DeveloperName = 'Pillar' LIMIT 1];

        System.debug(pillar);
        director.ManagerId = pillar.id;
        director2.ManagerId = pillar.id;
        update director;
        update director2;

        System.runAs(admin){
            Account customer = new Account();
            customer.RecordTypeId = [SELECT id FROM RecordType WHERE SobjectType = 'Account' AND DeveloperName = 'Customer_Group'].id;
            customer.Name = 'Test Customer Group';
            customer.OwnerId = [SELECT id FROM User WHERE Username = 'ManagerUser@ferrara.com.smbdev'].id;
            insert customer;

            Account otherCustomer = new Account();
            otherCustomer.RecordTypeId = [SELECT id FROM RecordType WHERE SobjectType = 'Account' AND DeveloperName = 'Customer_Group'].id;
            otherCustomer.Name = 'Other Test Customer Group';
            otherCustomer.OwnerId = [SELECT id FROM User WHERE Username = 'OtherManagerUser@ferrara.com.smbdev'].id;
            insert otherCustomer;

            Account allOtherCustomer = new Account();
            allOtherCustomer.RecordTypeId = [SELECT id FROM RecordType WHERE SobjectType = 'Account' AND DeveloperName = 'Customer_Group'].id;
            allOtherCustomer.Name = 'Test Customer Group All Other';
            allOtherCustomer.OwnerId = [SELECT id FROM User WHERE Username = 'DirectorUser@ferrara.com.smbdev'].id;
            allOtherCustomer.Is_All_Other__c = TRUE;
            insert allOtherCustomer;

            Sales_Call__c call = new Sales_Call__c();
            call.Plan__c = 100;
            call.LE_Sales_Call__c = 200;
            call.S_O__c = 100;
            call.Date__c = Date.today().toStartOfMonth();
            call.Customer__c = customer.id;
            insert call;

            Sales_Call__c call2 = new Sales_Call__c();
            call2.Plan__c = 100;
            call2.LE_Sales_Call__c = 600;
            call2.S_O__c = 100;
            call2.Date__c = call.Date__c.addMonths(1).toStartOfMonth();
            call2.Customer__c = customer.id;
            insert call2;

            Sales_Call__c call3 = new Sales_Call__c();
            call3.Plan__c = 100;
            call3.LE_Sales_Call__c = 200;
            call3.S_O__c = 100;
            call3.Latest_Estimate__c = 20;
            call3.Date__c = Date.today().toStartOfMonth();
            call3.Customer__c = allOtherCustomer.id;
            insert call3;

            Sales_Call__c call4 = new Sales_Call__c();
            call4.Plan__c = 100;
            call4.LE_Sales_Call__c = 400;
            call4.S_O__c = 100;
            call4.Date__c = Date.today().toStartOfMonth();
            call4.Customer__c = otherCustomer.id;
            insert call4;

            Revenue_Call_Config__c config = new Revenue_Call_Config__c();
            config.Months_Displayed__c = 3;
            config.Name = 'Default';
            insert config;
        }

    }
    @IsTest
    public static void itShouldLoadDirectorsAndSubordinatesData(){
        User director = [SELECT id, Name, ManagerId FROM User WHERE Username = 'DirectorUser@ferrara.com.smbdev'];

        //directors should be able to see other directors' data
        SalesTrackingController controller = new SalesTrackingController();
        System.runAs(director){

            System.assertEquals(1, controller.userTree.size());
            System.assertEquals(0, controller.userTree.get(0).salesCalls.size());
            System.assertEquals(7, controller.userTree.get(0).childrenWrappers.size());
            //should be only one for current month
            System.assertEquals(1, controller.userTree.get(0).childrenWrappers.get(0).childrenWrappers.get(0).salesCalls.size());
            System.assertEquals(1, controller.userTree.get(0).childrenWrappers.get(1).childrenWrappers.get(0).salesCalls.size());
            controller.monthChoice = Integer.valueOf(controller.monthOptions.get(1).getValue());
            controller.changeMonth();
            System.assertEquals(1, controller.userTree.get(0).childrenWrappers.get(0).childrenWrappers.get(0).salesCalls.size());
        }
    }

    @IsTest
    public static void itShouldSaveLatestEstimate(){
        User manager = [SELECT id, Name, ManagerId FROM User WHERE Username = 'ManagerUser@ferrara.com.smbdev'];

        System.runAs(manager){

            SalesTrackingController controller = new SalesTrackingController();

            System.assertEquals(1, controller.userTree.size());
            System.assertEquals(1, controller.userTree.get(0).salesCalls.size());
            System.assertEquals(0, controller.userTree.get(0).childrenWrappers.size());

            Integer newEstimate = 500;
            for(Sales_Call__c sc : controller.userTree.get(0).salesCalls){
                sc.Latest_Estimate__c = newEstimate;
            }
            controller.save();

            System.assertEquals(newEstimate, [SELECT id, Latest_Estimate__c FROM Sales_Call__c WHERE (Date__c >= :controller.startDate AND Date__c < :controller.endDate) AND Customer__r.OwnerId = :UserInfo.getUserId()].get(0).Latest_Estimate__c);
            Decimal planTotal = controller.planTotal;
            Decimal leTotal = controller.LETotal;
            Decimal soTotal = controller.SOTotal;
            Decimal latestEstaimteTotal = controller.latestEstimateTotal;
            Decimal leMinus = controller.leMinusCallTotal;
            Decimal leDemand = controller.leDemandTotal;
        }
    }

    @IsTest
    public static void itShouldExportAsCSV(){

        User admin = [SELECT id FROM User WHERE EMail = 'AdminUser@ferrara.com.smbdev'];
        System.runAs(admin){
              SalesTrackingController controller = new SalesTrackingController();

        controller.exportAsCSV();
        }

    }

    @IsTest
    public static void itShouldNotDisplayAllDataForManagers(){
        User manager = [SELECT id, Name, ManagerId FROM User WHERE Username = 'ManagerUser@ferrara.com.smbdev'];

        System.runAs(manager){
            SalesTrackingController controller = new SalesTrackingController();
            System.assertEquals(1, controller.userTree.size());
            System.assertEquals(1, controller.userTree.get(0).salesCalls.size());
            System.assertEquals(0, controller.userTree.get(0).childrenWrappers.size());
        }
    }
}