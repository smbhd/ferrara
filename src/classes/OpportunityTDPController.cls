/**
 * Created by ChrisMatos on 2/17/2017.
 */

public with sharing class OpportunityTDPController {

    private String baseQuery;
    public List<Opportunity> opportunities{get;set;}
    public List<Opportunity> currentOpps{get;set;}

    public String orderBy{get;set;}
    public Boolean isAsc{get;set;}
    public String prevOrderBy;


    public Integer currentPage {get; set;}
    public List<Integer> pagesAvailable {get; set;}
    public Integer availablePages{get;set;}
    public Integer targetPage{get;set;}
    public static final Integer PAGE_SIZE= 50;
    public Integer newPageSize{get;set;}

    public Id oppRetailerRecordId{get;set;}

    public String managerChoice{get;set;}
    public String teamLeadChoice{get;set;}
    public String accountChoice{get;set;}
    public String brandChoice{get;set;}
    public List<SelectOption> managerOptions{get;set;}
    public List<SelectOption> teamLeadOptions{get;set;}
    public List<SelectOption> accountOptions{get;set;}
    public List<SelectOption> brandOptions{get;set;}


    public OpportunityTDPController(){
        oppRetailerRecordId = [SELECT id FROM RecordType WHERE SobjectType = 'Opportunity' AND DeveloperName = 'Retailer_Opportunity'].id;

        Set<String> oppFields = Schema.getGlobalDescribe().get('Opportunity').getDescribe().fields.getMap().keySet();

        baseQuery = 'SELECT id, AccountId,Item__c ,PY_TDP__c, Planned_TDP__c, Gain_Loss__c, Change_To_TDP__c, TDP__c,Base_Distribution_Status__c ,CloseDate, StageName, LE_v_PY_TDP__c, LE_v_Planned_TDP__c, Name, SKU_Number__c,Item_Description__c, Account.Name, Owner.Name, Item__r.Brand__c,Item__r.Pack__c, Item__r.Item_Code__c, Owner.ManagerId, Owner.Manager.Name FROM Opportunity';

        Id retailerRecTypeId = [SELECT id FROM RecordType WHERE SobjectType = 'Account' AND DeveloperName = 'Retailer'].id;
        baseQuery += ' WHERE RecordTypeId = \'' +oppRetailerRecordId + '\'';

        isAsc = false;
        orderBy = 'Account.Name';
        pagesAvailable = new List<Integer>();
        currentOpps = new List<Opportunity>();
        currentPage = 1;
        targetPage = 1;

        sortHeader();
        populateAllPicklistOptions();

    }
    public void sortHeader(){
        if(prevOrderBy != null && prevOrderBy == orderBy){
            isAsc = !isAsc;
        }
        else{
            isAsc = true;
        }
        prevOrderBy = orderBy;

        filterRecords();

    }
    public void populateAllPicklistOptions(){
        managerOptions = new List<SelectOption>();
        teamLeadOptions = new List<SelectOption>();
        accountOptions = new List<SelectOption>();
        brandOptions = new List<SelectOption>();




        for(AggregateResult ar : [SELECT AccountId, Account.Name accountName FROM Opportunity  WHERE RecordTypeId = :oppRetailerRecordId Group BY AccountId, Account.Name]) {
            accountOptions.add(new SelectOption(String.valueOf(ar.get('AccountId')), String.valueOf(ar.get('accountName'))));
        }
        for(AggregateResult ar : [SELECT OwnerId, Owner.Name OwnerName FROM Opportunity WHERE RecordTypeId = :oppRetailerRecordId Group BY OwnerId, Owner.Name]){
            managerOptions.add(new SelectOption(String.valueOf(ar.get('OwnerId')), String.valueOf(ar.get('OwnerName'))));
        }
        for(AggregateResult ar : [SELECT Owner.ManagerId TeamLeadId, Owner.Manager.Name TeamLeadName FROM Opportunity  WHERE Owner.ManagerId != null AND RecordTypeId = :oppRetailerRecordId Group BY Owner.ManagerId, Owner.Manager.Name]) {
            teamLeadOptions.add(new SelectOption(String.valueOf(ar.get('TeamLeadId')), String.valueOf(ar.get('TeamLeadName'))));
        }
        for(AggregateResult ar : [SELECT Item__r.Brand__c brand FROM Opportunity  WHERE Item__r.Brand__c != null AND RecordTypeId = :oppRetailerRecordId Group BY Item__r.Brand__c]) {
            brandOptions.add(new SelectOption(String.valueOf(ar.get('brand')), String.valueOf(ar.get('brand'))));
        }
        if(accountOptions.size() == 0){
            accountOptions.add(new SelectOption('', '--None--'));
        }
        else{
            accountOptions = Utils.selectOptionSortByLabel(accountOptions);
            accountOptions.add(0, new SelectOption('', '--None--'));
        }
        if(managerOptions.size() == 0){
            managerOptions.add(new SelectOption('', '--None--'));
        }
        else{
            managerOptions = Utils.selectOptionSortByLabel(managerOptions);
            managerOptions.add(0, new SelectOption('', '--None--'));
        }
        if(teamLeadOptions.size() == 0){
            teamLeadOptions.add(new SelectOption('', '--None--'));
        }
        else{
            teamLeadOptions = Utils.selectOptionSortByLabel(teamLeadOptions);
            teamLeadOptions.add(0, new SelectOption('', '--None--'));
        }
        if(brandOptions.size() == 0){
            brandOptions.add(new SelectOption('', '--None--'));
        }
        else{
            brandOptions = Utils.selectOptionSortByLabel(brandOptions);
            brandOptions.add(0, new SelectOption('', '--None--'));
        }
    }
    public String getWhereClause(){
        String whereClause = '';
        if(String.isNotBlank(managerChoice)){

            whereClause += ' AND';

            whereClause += ' OwnerId = \'' + managerChoice + '\'' ;
        }
        if(String.isNotBlank(teamLeadChoice)){

            whereClause += ' AND';

            whereClause += ' Owner.ManagerId = \'' + teamLeadChoice + '\'' ;
        }
        if(String.isNotBlank(accountChoice)){

            whereClause += ' AND';

            whereClause += ' AccountId = \'' + accountChoice + '\'' ;
        }
        if(String.isNotBlank(brandChoice)){

            whereClause += ' AND';

            whereClause += ' Item__r.Brand__c = \'' + brandChoice + '\'' ;
        }

        return whereClause;

    }

    public void addOpportunityLine(){
        Opportunity newOpp = new Opportunity();
        newOpp.RecordTypeId = oppRetailerRecordId;
        if(currentOpps.size() == 0){
            currentOpps.add(newOpp);
        }
        else{
            currentOpps.add(0, newOpp);
        }

    }
    public void filterRecords(){
        String query = baseQuery;
        System.debug(baseQuery);

        query += getWhereClause();

        if(orderBy != null){
            query += getOrderByString();
            query += ' NULLS LAST';
            query += ' LIMIT 500';

        }
        try{
            System.debug(query);
            opportunities =  (List<Opportunity>)Database.query(query);

            reloadAvailablePages();
            populateCurrentOpps();
            currentPage = 1;
            populateAllPicklistOptions();
        }
        catch(Exception e){
            System.debug(e.getMessage());
            System.debug(e.getStackTraceString());
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getMessage()));
        }

    }
    public String getOrderByString(){
        String orderByString;
        if(!String.isBlank(orderBy)){
            orderByString = ' Order By ' + orderBy;
        }
        else{
            return '';
        }
        if(isAsc){
            orderByString += ' ASC';
        }
        else{
            orderByString += ' DESC';
        }
        return orderByString;

    }

    public void reloadAvailablePages(){
        calculateAvailablePages();
        populateListOfPages();
    }

    public void calculateAvailablePages(){
        availablePages = opportunities.size() / PAGE_SIZE;
        if (Math.mod(opportunities.size(),PAGE_SIZE) > 0) {
            availablePages++;
        }
        System.debug(availablePages);

    }
    public void populateListOfPages(){
        pagesAvailable.clear();
        for (Integer i = 0; i < availablePages; i++) {
            pagesAvailable.add(i);
        }
    }
    public void populateCurrentOpps(){
        currentOpps.clear();
        for (Integer i = 0; i < PAGE_SIZE;  i++) {
            if (i < opportunities.size()) {
                currentOpps.add(opportunities.get(i));
            }
        }
    }

    public PageReference nextPage() {
        System.debug(currentPage);
        System.debug(pagesAvailable);
        if (currentPage + 1 <= pagesAvailable.size()) {
            currentOpps.clear();
            currentPage++;
            for (Integer i = PAGE_SIZE * currentPage - PAGE_SIZE; i < PAGE_SIZE * currentPage; i++) {
                if (i < opportunities.size()) {
                    currentOpps.add(opportunities.get(i));
                }
            }
        }
        return null;
    }

    public PageReference previousPage() {
        if (currentPage - 1 > 0) {
            currentOpps.clear();
            currentPage--;
            for (Integer i = PAGE_SIZE * currentPage - PAGE_SIZE; i < PAGE_SIZE * currentPage; i++) {
                if (i < opportunities.size()) {
                    currentOpps.add(opportunities.get(i));
                }
            }
        }
        return null;
    }
    public PageReference goToPage() {
        if (ApexPages.currentPage().getParameters().get('targetPage') != null) {
            targetPage = Integer.valueOf(ApexPages.currentPage().getParameters().get('targetPage'));
        }
        if (targetPage > 0 && targetPage <= pagesAvailable.size()) {
            currentOpps.clear();
            currentPage = targetPage;
            for (Integer i = PAGE_SIZE * currentPage - PAGE_SIZE; i < PAGE_SIZE * currentPage; i++) {
                if (i < opportunities.size()) {
                    currentOpps.add(opportunities.get(i));
                }
            }
        }
        return null;
    }
    public PageReference cancel(){
        return new PageReference('/');
    }
    public PageReference exportCSV(){
        List<Opportunity> opportunitiesToExport;
        String query = baseQuery;

        query += getWhereClause();

        if(orderBy != null){
            query += getOrderByString();
            query += ' NULLS LAST';
        }
        try{
            System.debug(query);
           opportunitiesToExport =  (List<Opportunity>)Database.query(query);
        }
        catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getMessage()));
            System.debug(e.getMessage());
            System.debug(e.getStackTraceString());
            return null;
        }

        Integer rows = 0;
        String attachmentBody = '';
        final String OWNER = 'Manager';
        final String TEAM_LEAD = 'Team Lead';
        final String RETAILER = 'Retailer';
        final String STAGE = 'Status';
        final String CLOSE_DATE = 'Timing';
        final String BRAND = 'Brand';
        final String PACK = 'Pack Type';

        if(opportunitiesToExport == null || opportunitiesToExport.size() == 0){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Nothing to export'));
            return null;
        }
        List<String> apiNames = new List<String>{TEAM_LEAD, OWNER, RETAILER, 'SKU_Number__c', 'Item_Description__c', 'PY_TDP__c','Planned_TDP__c', 'Change_To_TDP__c','TDP__c', CLOSE_DATE, STAGE, 'LE_v_PY_TDP__c', 'LE_v_Planned_TDP__c', BRAND, PACK};
        List<String> headerValues = new List<String>();
        Map<String, SObjectField> fieldMap = Schema.getGlobalDescribe().get('Opportunity').getDescribe().fields.getMap();

        for(String apiName : apiNames){
            if(fieldMap.containsKey(apiName)){
                headerValues.add(fieldMap.get(apiName).getDescribe().getLabel());
            }
            else{
                headerValues.add(apiName);
            }

        }
        System.debug(headerValues);

        attachmentBody += String.join(headerValues, ',');
        attachmentBody += '\n';
        for(Opportunity opp : opportunitiesToExport){
            for(String apiName : apiNames){
                if(apiName == OWNER){
                    if(opp.OwnerId != null && String.isNotBlank(opp.Owner.Name) ){
                        String value = opp.Owner.Name;
                        value = value.escapeCsv();
                        attachmentBody += value;
                    }
                }
                else if(apiName == TEAM_LEAD){
                    if(opp.Owner.ManagerId != null && String.isNotBlank(opp.Owner.Manager.Name) ){
                        String value = opp.Owner.Manager.Name;
                        value = value.escapeCsv();
                        attachmentBody += value;
                    }
                }
                else if(apiName == RETAILER){
                    if(opp.AccountId != null && String.isNotBlank(opp.Account.Name) ){
                        String value = opp.Account.Name;
                        value = value.escapeCsv();
                        attachmentBody += value;
                    }
                }
                else if(apiName == CLOSE_DATE){
                    if(opp.CloseDate != null ){
                        String value = opp.CloseDate.format();
                        value = value.escapeCsv();
                        attachmentBody += value;
                    }
                }
                else if(apiName == STAGE){
                    if(opp.StageName != null ){
                        String value = opp.StageName;
                        value = value.escapeCsv();
                        attachmentBody += value;
                    }
                } else if(apiName == BRAND){
                    if(opp.Item__r.Brand__c != null){
                        String value = opp.Item__r.Brand__c;
                        value =  value.escapeCsv();
                        attachmentBody += value;
                    }
                } else if(apiName == PACK){
                    if(opp.Item__r.Pack__c != null){
                        String value = opp.Item__r.Pack__c;
                        value = value.escapeCsv();
                        attachmentBody += value;
                    }
                } else{
                    if(opp.get(apiName) != null){
                        attachmentBody += String.valueOf(opp.get(apiName)).escapeCsv();
                    }
                }
                attachmentBody += ',';
            }
            attachmentBody += '\n';
            rows++;
        }
        if(rows == 0){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'No rows to export'));
            return null;
        }
        System.debug(attachmentBody);
        Savepoint sp = Database.setSavepoint();
        try{
            CSV_Export__c csv_export = new CSV_Export__c();
            insert csv_export;
            Attachment att = new Attachment();
            att.Name = 'TDP Input Export:' + Date.today().format() + '.csv';
            att.Body = Blob.valueOf(attachmentBody);
            att.ParentId = csv_export.id;
            att.ContentType = 'text/csv';
            insert att;

            return new PageReference('/servlet/servlet.FileDownload?file=' + att.id);
        }
        catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getMessage()));
            System.debug(e.getMessage());
            System.debug(e.getStackTraceString());
            Database.rollback(sp);
            return null;
        }
    }
    public void save(){
        try{

            update opportunities;
            List<Opportunity> newOpps = new List<Opportunity>();
            for(Opportunity opp : currentOpps){
                if(opp.id == null){
                    opp.Name = 'PLACEHOLDER';
                    newOpps.add(opp);
                }

            }

            insert newOpps;
            filterRecords();
        }
        catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getMessage()));
            System.debug(e.getMessage());
            System.debug(e.getStackTraceString());
            return;
        }
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Records Updated'));
        return;

    }
    private void populateNamesOfOpps(List<Opportunity> opps){
        if(opps == null || opps.size() == 0){
            return;
        }
        Map<Id, String> accountNamesById = new Map<Id, String>();
        Map<Id, String> itemCodeById = new Map<Id, String>();
        for(Opportunity opp : opps){
            accountNamesById.put(opp.AccountId, null);
            if(opp.Item__c != null){
                itemCodeById.put(opp.Item__c, null);
            }
        }
        for(Account acc : [SELECT id, Name FROM Account WHERE id IN :accountNamesById.keySet()]){
            accountNamesById.put(acc.id, acc.Name);
        }
        for(Product2 prod : [SELECT id, Item_Code__c FROM Product2 WHERE Id IN :itemCodeById.keySet()]){
            itemCodeById.put(prod.id, prod.Item_Code__c);
        }
        for(Opportunity opp : opps){
           String name = '';
            if(accountNamesById.containsKey(opp.AccountId)){
                name += accountNamesById.get(opp.AccountId);
            }
            if(itemCodeById.containsKey(opp.Item__c)){
                name += ' - ' + itemCodeById.get(opp.Item__c);
            }
            opp.Name = name;
        }
    }
}