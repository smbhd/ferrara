/**
 * Created by ChrisMatos on 11/1/2016.
 */

public with sharing class OpportunityBulkEditController {

    public List<Opportunity> allOpportunities{get;set;}
    public List<Opportunity> currentOpps{get;set;}
    public List<SelectOption> viewOptions{get;set;}
    public List<SelectOption> accountOptions{get;set;}
    public List<SelectOption> salesManagersOptions{get;set;}

    public String managerIdChoice{get;set;}
    public String accTypeChoice{get;set;}
    public String viewChoice{get;set;}
    public Integer nextYear{get;set;}
    public String orderBy{get;set;}
    public String prevOrderBy;
    public Boolean isAsc{get;set;}

    public Id oppRecordId{get;set;}
    public Id riskRecordId{get;set;}

    private Id custGroupRecTypeId;
    private Id retailerRecTypeId;

    public Integer currentPage {get; set;}
    public List<Integer> pagesAvailable {get; set;}
    public Integer availablePages{get;set;}
    public Integer targetPage{get;set;}
    public static final Integer ENTRIES = 50;
    public String filterLetter{get;set;}
    private String ascii{
        get {
            if(ascii == null){
                ascii = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            }

            return ascii;

        }
        set{}
    }
    public List<String> asciiStrings{get;set;}

    private String baseQuery;

    public OpportunityBulkEditController(){
        //initializations
        oppRecordId = [SELECT id FROM RecordType WHERE SobjectType = 'Opportunity' AND DeveloperName = 'Opportunity'].id;
        riskRecordId = [SELECT id FROM RecordType WHERE SobjectType = 'Opportunity' AND DeveloperName = 'Risk'].id;

        custGroupRecTypeId = [SELECT id FROM RecordType WHERE SobjectType = 'Account' AND DeveloperName = 'Customer_Group'].id;
        retailerRecTypeId = [SELECT id FROM RecordType WHERE SobjectType = 'Account' AND DeveloperName = 'Retailer'].id;
        initAsciiStrings();
        nextYear = System.today().year() + 1;
        isAsc = false;
        currentOpps = new List<Opportunity>();
        pagesAvailable = new List<Integer>();
        currentPage = 1;
        targetPage = 1;

        //setup view options
        List<String> viewOptStrings = new List<String>{'All R&O\'s','All Opportunities', 'All Risks','New This Week', 'Decisions This Month','Decisions Next Month',  'Won' };
        viewOptions = new List<SelectOption>();
        for(String viewOpt : viewOptStrings){
            viewOptions.add(new SelectOption(viewOpt, viewOpt));
        }
        viewChoice = viewOptions.get(0).getValue();

        //init opps
        baseQuery = 'SELECT id, Name, Account.Name,Account.RecordTypeId, Amount, CloseDate, TDP__c, Description, Owner.Name, StageName, AccountId, Probability__c, RecordTypeId, Item__c,Item__r.Name, Item_Description__c, RecordType.Name FROM Opportunity';
        baseQuery += ' WHERE Account.RecordTypeId = \'' + custGroupRecTypeId + '\'';
        allOpportunities = (List<Opportunity>)Database.query(baseQuery);

        populateSalesManagerOptions();
        reloadAvailablePages();
        populateCurrentOpps();


    }
    public void populateSalesManagerOptions(){
        salesManagersOptions = new List<SelectOption>();
        salesManagersOptions.add(new SelectOption('', 'All'));
        Map<Id, String> salesManagersByIds = new Map<Id, String>();
        for(Opportunity opp : allOpportunities){
            if(!salesManagersByIds.containsKey(opp.OwnerId)){
                salesManagersByIds.put(opp.OwnerId, opp.Owner.name);
            }
        }

        for(Id key : salesManagersByIds.keySet()){
            salesManagersOptions.add(new SelectOption(key,salesManagersByIds.get(key)));
        }

    }
    public void initAsciiStrings(){
        asciiStrings = new List<String>();
        for(Integer i = 0; i < ascii.length(); i++){
            asciiStrings.add(ascii.substring(i, i + 1));
        }
        System.debug(asciiStrings);
    }
    public void reloadAvailablePages(){
        calculateAvailablePages();
        populateListOfPages();
    }

    public void calculateAvailablePages(){
        availablePages = allOpportunities.size() / ENTRIES;
        if (Math.mod(allOpportunities.size(),ENTRIES) > 0) {
            availablePages++;
        }

    }
    public void populateListOfPages(){
        pagesAvailable.clear();
        for (Integer i = 0; i < availablePages; i++) {
            pagesAvailable.add(i);
        }
    }
    public void populateCurrentOpps(){
        currentOpps.clear();
        for (Integer i = 0; i < ENTRIES;  i++) {
            if (i < allOpportunities.size()) {
                currentOpps.add(allOpportunities.get(i));
            }
        }
    }
    public void filterByView(){
        filterLetter = null;
        filterRecords();
    }
    public void sortHeader(){
        filterLetter = null;
        if(prevOrderBy != null && prevOrderBy == orderBy){
            isAsc = !isAsc;
        }
        else{
            isAsc = true;
        }
        prevOrderBy = orderBy;

        filterRecords();

    }

    public void filterRecords(){
        String query = baseQuery;
        if(viewChoice != null){
            query += getWhereClause();
        }
        if(orderBy != null){
            query += getOrderByString();
            query += ' NULLS LAST';

        }
        try{
            System.debug(query);
            allOpportunities =  (List<Opportunity>)Database.query(query);
            reloadAvailablePages();
            populateCurrentOpps();
        }
        catch(Exception e){
            System.debug(e.getMessage());
            System.debug(e.getStackTraceString());
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getMessage()));
        }
        System.debug(query);

    }

    public String getWhereClause(){
        String whereClause;
        System.debug(filterLetter);
        System.debug(viewChoice);
        if(viewChoice == 'All R&O\'s'){
            whereClause = '';
        }
        else if(viewChoice == 'Decisions This Month'){
            whereClause = ' AND isClosed = FALSE AND CloseDate = THIS_MONTH';
        }
        else if(viewChoice == 'Won'){
            whereClause = ' AND IsWon=TRUE';
        }
        else if(viewChoice == 'Decisions Next Month'){
            whereClause = ' AND isClosed = FALSE AND CloseDate = NEXT_MONTH';
        }
        else if(viewChoice == 'New This Week'){
            whereClause = ' AND CreatedDate = THIS_WEEK';
        }
        else if(viewChoice == 'My R&O\'s'){
            whereClause = ' AND OwnerId = \'' + UserInfo.getUserId() + '\'';
        }
        else if(viewChoice == 'All Opportunities'){
            whereClause = ' AND RecordTypeId = \'' + oppRecordId + '\'';
        }
        else if(viewChoice == 'All Risks'){
            whereClause = ' AND RecordTypeId = \'' + riskRecordId + '\'';
        }
        else{
            return '';
        }
        if(filterLetter != null && filterLetter != ''){

            whereClause += ' AND';

            whereClause += getLetterFilter();
        }
        if(managerIdChoice != null && managerIdChoice != ''){

            whereClause += ' AND';

            whereClause += ' OwnerId = \'' + managerIdChoice + '\'' ;
        }

        return whereClause;

    }

    public String getLetterFilter(){
        String letterFilter;
        if(filterLetter != null && filterLetter != ''){
            if(orderBy == null || orderBy == 'Name'){
                letterFilter = ' NAME LIKE \'';
            }
            else if(orderBy == 'Account'){
                letterFilter =  ' Account.NAME LIKE \'';
            }
            else if(orderBy == 'Stage'){
                letterFilter =  ' StageName LIKE \'';
            }
            else if(orderBy == 'Owner'){
                letterFilter =  ' Owner.Name LIKE \'';
            }
            else if(orderBy == 'Probability'){
                letterFilter =  ' Probability__c LIKE \'';
            }
            else{
                return '';
            }
            letterFilter += String.escapeSingleQuotes(filterLetter) + '%\'';
            return letterFilter;
        }
        else{
            return '';
        }
    }
    public String getOrderByString(){
        String orderByString;
        if(orderBy == 'Name'){
            orderByString = ' Order By Name';
        }
        else if(orderBy == 'Account'){
            orderByString =  ' Order By Account.Name';
        }
        else if(orderBy == 'Amount'){
            orderByString = ' Order By Amount';
        }
        else if(orderBy == 'CloseDate'){
            orderByString = ' Order By CloseDate';
        }
        else if(orderBy == 'Stage'){
            orderByString = ' Order By StageName';
        }
        else if(orderBy == 'Owner'){
            orderByString = ' Order By Owner.Name';
        }
        else if(orderBy == 'Probability'){
            orderByString = ' Order By Probability__c';
        }
        else if(orderBy == 'Record Type'){
            orderByString = ' Order By RecordType.Name';
        }
        else{
            return '';
        }
        if(isAsc){
            orderByString += ' ASC';
        }
        else{
            orderByString += ' DESC';
        }
        return orderByString;

    }
    public PageReference nextPage() {
        System.debug(currentPage);
        System.debug(pagesAvailable);
        if (currentPage + 1 <= pagesAvailable.size()) {
            currentOpps.clear();
            currentPage++;
            for (Integer i = ENTRIES * currentPage - ENTRIES; i < ENTRIES * currentPage; i++) {
                if (i < allOpportunities.size()) {
                    currentOpps.add(allOpportunities.get(i));
                }
            }
        }
        return null;
    }

    public PageReference previousPage() {
        if (currentPage - 1 > 0) {
            currentOpps.clear();
            currentPage--;
            for (Integer i = ENTRIES * currentPage - ENTRIES; i < ENTRIES * currentPage; i++) {
                if (i < allOpportunities.size()) {
                    currentOpps.add(allOpportunities.get(i));
                }
            }
        }
        return null;
    }
    public PageReference goToPage() {
        if (ApexPages.currentPage().getParameters().get('targetPage') != null) {
            targetPage = Integer.valueOf(ApexPages.currentPage().getParameters().get('targetPage'));
        }
        if (targetPage > 0 && targetPage <= pagesAvailable.size()) {
            currentOpps.clear();
            currentPage = targetPage;
            for (Integer i = ENTRIES * currentPage - ENTRIES; i < ENTRIES * currentPage; i++) {
                if (i < allOpportunities.size()) {
                    currentOpps.add(allOpportunities.get(i));
                }
            }
        }
        return null;
    }
    public PageReference goToNewOpp(){
        PageReference newOppPage = new PageReference('/006/e');
        newOppPage.getParameters().put('RecordType', oppRecordId);
        newOppPage.getParameters().put('retURL',Page.OpportunityBulkEdit.getUrl() );
        return newOppPage;
    }
    public PageReference goToNewRisk(){
        PageReference newRiskPage = new PageReference('/006/e');
        newRiskPage.getParameters().put('RecordType', riskRecordId);
        PageReference bulkEditPage = Page.OpportunityBulkEdit;
        newRiskPage.getParameters().put('retURL',bulkEditPage.getUrl() );
        return newRiskPage;
    }
    public PageReference cancel(){
        return new PageReference('/');
    }
    public void save(){
        try{
//            for(Opportunity opp : allOpportunities){
//                if(opp.RecordTypeId == riskRecordId && opp.Amount > 0){
//                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Risks must have a negative Revenue Impact'));
//                    return;
//                }
//                else if(opp.RecordTypeId == oppRecordId && opp.Amount < 0){
//                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Opportunities must have a positive Revenue Impact'));
//                    return;
//                }
//            }
            update allOpportunities;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Records Updated'));

        }
        catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getMessage()));
            System.debug(e.getMessage());
            System.debug(e.getStackTraceString());
        }


    }
}