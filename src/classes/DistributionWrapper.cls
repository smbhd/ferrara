/**
 * Created by ChrisMatos on 2/9/2017.
 */

global class DistributionWrapper {

    public Map<String, Decimal> leTDPByAccountId{get;set;}
    public Map<String, Decimal> pendingTDPByAccountId{get;set;}
    public List<Id> productIds;
    public String pack{get;set;}
    public String description{get;set;}

    public DistributionWrapper(String name){
        productIds = new List<Id>();
        description = name;
        leTDPByAccountId = new Map<String, Decimal>();
        pendingTDPByAccountId = new Map<String, Decimal>();
    }

}