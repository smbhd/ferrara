/**
 * Created by ChrisMatos on 3/10/2017.
 */
@IsTest
public with sharing class SathersTrackerControllerTest {
    @TestSetup
    public static void setup(){
        Account cust_group = new Account();
        cust_group.Name = 'Test Customer Group';
        cust_group.RecordTypeId = [SELECT id FROM RecordType WHERE DeveloperName = 'Customer_Group' LIMIT 1].id;
        insert cust_group;
    }
    @IsTest
    public static void itShouldSave(){
        SathersTrackerController controller = new SathersTrackerController();
        System.assertEquals(2, controller.ownerOptions.size());
        String ownerId = controller.ownerOptions.get(1).getValue();
        controller.ownerChoice = ownerId;
        controller.filterByOwner();
        System.assertEquals(1, controller.currentAccounts.size());
        Account acc = controller.currentAccounts.get(0);

        acc.X2_for_5__c = 5;
        acc.X2_for_3__c = 3;

        controller.save();
        controller.filterByManager();
        controller.updateOwnerOptionList();

        Account account = [SELECT id, X2_for_3__c, X2_for_5__c FROM Account WHERE Name = 'Test Customer Group'];
        System.assertEquals(5, account.X2_for_5__c);
        System.assertEquals(3, account.X2_for_3__c);
    }

    @IsTest
    public static void itShouldSupportMultiplePages(){
        List<Account> accounts = new List<Account>();
        Id custGroupRecordType = [SELECT id FROM RecordType WHERE DeveloperName = 'Customer_Group' LIMIT 1].id;
        for(Integer i = 0; i < SathersTrackerController.ENTRIES + 1; i++){
            Account acc = new Account();
            acc.RecordTypeId = custGroupRecordType;
            acc.Name = 'Test Account ' + i;
            accounts.add(acc);
        }
        insert accounts;
        SathersTrackerController controller = new SathersTrackerController();
        System.assertEquals(2, controller.availablePages);
        controller.nextPage();
        System.assertEquals(2, controller.currentPage);
        controller.previousPage();
        System.assertEquals(1, controller.currentPage);
        controller.targetPage = 2;
        controller.goToPage();
        System.assertEquals(2, controller.currentPage);

    }
}