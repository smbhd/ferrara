/**
 * Created by ChrisMatos on 11/3/2016.
 */

@IsTest
public with sharing class AccountBulkEditControllerTest {

    @TestSetup
    public static void testSetup(){
        Account retailer = new Account();
        retailer.Name = 'SMB Help Desk';
        retailer.RecordTypeId = [SELECT id FROM RecordType WHERE DeveloperName='Retailer' LIMIT 1].id;
        insert retailer;

        Account cust_group = new Account();
        cust_group.Name = 'Test Customer Group';
        cust_group.RecordTypeId = [SELECT id FROM RecordType WHERE DeveloperName = 'Customer_Group' LIMIT 1].id;
        insert cust_group;

    }
    @IsTest
    public static void itShouldPullInNextAndPreviousTasksOnAccounts(){
        Account acc = [SELECT id, Name FROM Account WHERE Name = 'SMB Help Desk'];
        Task nextMeeting = new Task();
        nextMeeting.WhatId = acc.id;
        nextMeeting.Subject = 'Next Meeting';
        nextMeeting.Description = 'comments';
        nextMeeting.ActivityDate = System.today().addMonths(1);
        insert nextMeeting;

        Task notNextMeeting = new Task();
        notNextMeeting.WhatId = acc.id;
        notNextMeeting.Subject = 'Not Next Meeting';
        notNextMeeting.Description = 'comments';
        notNextMeeting.ActivityDate = System.today().addMonths(2);
        insert notNextMeeting;

        Task previousMeeting = new Task();
        previousMeeting.WhatId = acc.id;
        previousMeeting.Subject = 'Previous Meeting';
        previousMeeting.Description = 'comments';
        previousMeeting.ActivityDate = System.today().addMonths(-1);
        previousMeeting.Status = 'Completed';
        insert previousMeeting;

        Task notPreviousMeeting = new Task();
        notPreviousMeeting.WhatId = acc.id;
        notPreviousMeeting.Subject = 'Not Previous Meeting';
        notPreviousMeeting.Description = 'comments';
        notPreviousMeeting.ActivityDate = System.today().addMonths(-2);
        notPreviousMeeting.Status = 'Completed'; 
        insert notPreviousMeeting;

        AccountBulkEditController controller = new AccountBulkEditController();
        controller.orderBy = 'Name';
        controller.refreshQueryAndLoadTable();
        
        AccountBulkEditController.MeetingWrapper smbWrapper;
        for(AccountBulkEditController.MeetingWrapper mw : controller.allMeetingWrappers){
            if(mw.acc.Name == 'SMB Help Desk'){
                smbWrapper = mw;
            }
        }
        
        System.assertEquals(nextMeeting.Subject, smbWrapper.nextTask.Subject);
        System.assertEquals(previousMeeting.Subject, smbWrapper.previousTask.Subject);

        //pagination test
        controller.nextPage();
        System.assertEquals(1, controller.currentPage);
        controller.previousPage();
        System.assertEquals(1, controller.currentPage);
        controller.targetPage = 2;
        controller.goToPage();
        System.assertEquals(1, controller.currentPage);

        AccountBulkEditController.sortBy = 'Next';
        controller.sortByMeeting();
        AccountBulkEditController.sortBy = 'Previous';
        controller.sortByMeeting();
		
        controller.exportCSV();

        controller.sortHeader();

    }
    @IsTest
    public static void itShouldSaveChanges(){

        AccountBulkEditController controller = new AccountBulkEditController();
        controller.allMeetingWrappers.get(0).acc.Meeting_Comments__c = 'Test Comment';
        controller.save();
        controller.cancel();

        Account acc = [SELECT id, Name, Meeting_Comments__c FROM Account WHERE Name = 'SMB Help Desk'];

        System.assertEquals('Test Comment', acc.Meeting_Comments__c);
    }

    @IsTest
    public static void itShouldSupportMultiplePages(){
        List<Account> accounts = new List<Account>();
        Id custGroupRecordType = [SELECT id FROM RecordType WHERE DeveloperName = 'Customer_Group' LIMIT 1].id;
        for(Integer i = 0; i < AccountBulkEditController.ENTRIES + 1; i++){
            Account acc = new Account();
            acc.RecordTypeId = custGroupRecordType;
            acc.Name = 'Test Account ' + i;
            accounts.add(acc);
        }
        insert accounts;
        AccountBulkEditController controller = new AccountBulkEditController();
        System.assertEquals(2, controller.availablePages);
        controller.nextPage();
        System.assertEquals(2, controller.currentPage);
        controller.previousPage();
        System.assertEquals(1, controller.currentPage);
        controller.targetPage = 2;
        controller.goToPage();
        System.assertEquals(2, controller.currentPage);

    }
    @isTest
    public static void itShouldExportAsCSV(){
         AccountBulkEditController controller = new AccountBulkEditController();
        controller.exportCSV();
    }


}