/**
 * Created by AlexSanborn on 5/19/2017.
 */

public class SalesTrackingDirectorController {
    public static Map<Id, Map<Integer, List<Sales_Call__c>>> getSalesCallMapAllAccounts(Date startDate, Date endDate){
        Map<Id, Map<Integer, List<Sales_Call__c>>> salesCallsByAccountOwnerIdByMonth = new Map<Id, Map<Integer, List<Sales_Call__c>>>();

        for(Sales_Call__c sc : [SELECT id, LE_Sales_Call__c, Plan__c,Replan__c, Latest_Estimate__c, Replan_Minus_Sales_Call__c, LE_Demand_Forecast__c, Customer__r.Name, S_O__c, Date__c, Customer__r.OwnerId, LE_Minus_Sales_Call__c, Customer__r.Is_All_Other__c FROM Sales_Call__c WHERE (Date__c >= :startDate AND Date__c < :endDate) ORDER BY LE_Sales_Call__c DESC]){
            if(!salesCallsByAccountOwnerIdByMonth.containsKey(sc.Customer__r.OwnerId)){
                salesCallsByAccountOwnerIdByMonth.put(sc.Customer__r.OwnerId, new Map<Integer, List<Sales_Call__c>>());
            }
            if(!salesCallsByAccountOwnerIdByMonth.get(sc.Customer__r.OwnerId).containsKey(sc.Date__c.month())){
                salesCallsByAccountOwnerIdByMonth.get(sc.Customer__r.OwnerId).put(sc.Date__c.month(), new List<Sales_Call__c>());
            }
            if(sc.Latest_Estimate__c == null){
                sc.Latest_Estimate__c = sc.LE_Demand_Forecast__c;
            }
            salesCallsByAccountOwnerIdByMonth.get(sc.Customer__r.OwnerId).get(sc.Date__c.month()).add(sc);
        }
        return salesCallsByAccountOwnerIdByMonth;
    }
}