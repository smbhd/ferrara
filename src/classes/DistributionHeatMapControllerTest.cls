/**
 * Created by ChrisMatos on 2/9/2017.
 */
@IsTest
public class DistributionHeatMapControllerTest {
    @TestSetup
    public static void setup(){
        Account a = new Account();
        a.Name = 'Test Account';
        a.RecordTypeId = [SELECT id FROM RecordType WHERE SobjectType = 'Account' AND DeveloperName = 'Retailer'].id;
        insert a;

        Product2 item = new Product2();
        item.Name = 'Must Item';
        item.Heat_Map_Group__c = 'Must Item';
        item.Stock__c = 'Must';
        item.Heat_Map_Pack_Group__c = 'Peg';
        item.Item_Code__c = String.valueOf(Math.mod(Math.random().intValue(), 1000));
        item.UPC__c = '12345';
        item.IsActive = True;
        insert item;
        Product2 item2 = new Product2();
        item2.Name = 'Should Item';
        item2.Heat_Map_Group__c = 'Should Item';
        item2.Stock__c = 'Should';
        item2.Heat_Map_Pack_Group__c = 'Count Goods';
        item2.Item_Code__c = String.valueOf(Math.mod(Math.random().intValue(), 1000));
        item2.UPC__c = '12345';
        item2.IsActive = True;
        insert item2;


        Opportunity opp = new Opportunity();
        opp.Name = 'Test opp';
        opp.AccountId = a.id;
        opp.Item__c = item.id;
        opp.StageName = 'Pending';
        opp.CloseDate = Date.today();
        opp.TDP__c = 100;
        insert opp;

        Opportunity opp2 = new Opportunity();
        opp2.Name = 'Test opp';
        opp2.AccountId = a.id;
        opp2.Item__c = item2.id;
        opp2.StageName = 'Pending';
        opp2.CloseDate = Date.today();
        opp2.TDP__c = 200;
        insert opp2;
    }

    @IsTest
    public static void itShouldGroupProductsOfSameNameAndPack(){
        DistributionHeatMapController controller = new DistributionHeatMapController();

        System.assertEquals(2, controller.productsOnHeatMap.size());
        System.assertEquals(1, controller.mustStockWrappers.size());
        System.assertEquals(1, controller.shouldStockWrappers.size());
    }
    @IsTest
    public static void itShouldAllowFilters(){
        User director = new User();
        director.Alias = 'DirectU';
        director.Email = 'DirectorUser@ferrara.com.smbdev1';
        director.EmailEncodingKey = 'UTF-8';
        director.LastName = 'Director';
        director.LanguageLocaleKey = 'en_US';
        director.ProfileId = [SELECT id FROM Profile WHERE Name = 'Standard User'].id;
        director.UserRoleId = [SELECT id FROM UserRole WHERE DeveloperName = 'Sales_Director' LIMIT 1].id;
        director.TimeZoneSidKey = 'America/Chicago';
        director.Username = 'DirectorUser@ferrara.com.smbdev1';
        director.LocaleSidKey = 'en_US';

        User manager = new User();
        manager.Alias = 'manU';
        manager.Email = 'ManagerUser@ferrara.com.smbdev1';
        manager.EmailEncodingKey = 'UTF-8';
        manager.LastName = 'Manager';
        manager.LanguageLocaleKey = 'en_US';
        manager.ProfileId = [SELECT id FROM Profile WHERE Name = 'Standard User'].id;
        manager.UserRoleId = [SELECT id FROM UserRole WHERE DeveloperName = 'Sales_Manager' LIMIT 1].id;
        manager.TimeZoneSidKey = 'America/Chicago';
        manager.Username = 'ManagerUser@ferrara.com.smbdev1';
        manager.LocaleSidKey = 'en_US';

        List<User> users = new List<User>{director, manager};
        insert users;

        manager.ManagerId = director.id;
        update manager;

        System.runAs(manager){
            Account a = new Account();
            a.Name = 'Test manU Account';
            a.RecordTypeId = [SELECT id FROM RecordType WHERE SobjectType = 'Account' AND DeveloperName = 'Retailer'].id;
            insert a;
            Opportunity opp = new Opportunity();
            opp.Name = 'Test manU Opp';
            opp.AccountId = a.id;
            opp.Item__c = [SELECT id FROM Product2 WHERE Name = 'Should Item' LIMIT 1].id;
            opp.StageName = 'Pending';
            opp.CloseDate = Date.today();
            opp.TDP__c = 1000;
            insert opp;

        }

        DistributionHeatMapController controller = new DistributionHeatMapController();
        System.assertEquals(2, controller.accountsByStock.get(DistributionHeatMapController.SHOULD_STOCK).size());
        System.assertEquals(2, controller.teamLeadOptionsbyStock.get(DistributionHeatMapController.SHOULD_STOCK).size());
        controller.teamLeadShouldChoice =  controller.teamLeadOptionsbyStock.get(DistributionHeatMapController.SHOULD_STOCK).get(1).getValue();
        controller.populateAccounts();
        System.assertEquals(1, controller.accountsByStock.get(DistributionHeatMapController.SHOULD_STOCK).size());

    }
}