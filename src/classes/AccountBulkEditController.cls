public with sharing class  AccountBulkEditController {

    public List<MeetingWrapper> allMeetingWrappers{get;set;}
    public List<MeetingWrapper> currentMeetingWrappers{get;set;}
    public List<Account> allAccounts;

    public Integer currentPage {get; set;}
    public List<Integer> pagesAvailable {get; set;}
    public Integer availablePages{get;set;}
    public Integer targetPage{get;set;}
    public static final Integer ENTRIES = 50;

    public String managerIdChoice{get;set;}
    public String teamLeadChoice{get;set;}
    public String accountChoice{get;set;}
    public List<SelectOption> salesManagersOptions{get;set;}
    public List<SelectOption> teamLeadOptions{get;set;}
    public List<SelectOption> accountOptions{get;set;}

    public String orderBy{get;set;}
    public String previousOrderBy{get;set;}
    public Boolean isAsc{get;set;}
    public static Boolean meetingIsAsc{get;set;}

    public static String sortBy{get;set;}

    private Map<Id, Task> previousMeetingByAccountId;
    private Map<Id, Task> nextMeetingByAccountId;
    private String baseQuery;

    public AccountBulkEditController(){


        baseQuery = 'SELECT id, Pillar__c, Sales_District_Description__c, Meeting_Comments__c, Customer_ID__c, Name, CSR__c, Broker__c, Phone, Fax, RecordType.Name,Owner.Name, Store_Count__c, BillingState, IRI__c, OwnerId, Owner.ManagerId, Owner.Manager.Name FROM Account';
        RecordType custGroupRecType = [SELECT id, DeveloperName FROM RecordType WHERE DeveloperName = 'Customer_Group'];
        RecordType retailerRecType = [SELECT id, DeveloperName FROM RecordType WHERE DeveloperName = 'Retailer'];
        baseQuery += ' WHERE (RecordTypeId = \'' + custGroupRecType.id + '\' OR RecordTypeId = \'' + retailerRecType.id + '\')';
        initAccounts();

        allMeetingWrappers = new List<MeetingWrapper>();
        currentPage = 1;
        targetPage = 1;
        isAsc = true;
        orderBy = 'Name';
        previousOrderBy = 'Name';
        previousMeetingByAccountId = new Map<Id, Task>();
        nextMeetingByAccountId = new Map<Id, Task>();

        for(Task aTask : [SELECT id, Description, AccountId, ActivityDate, Subject, isClosed FROM Task WHERE AccountId IN :allAccounts AND IsClosed = TRUE ORDER BY ActivityDate DESC]){
            if(!previousMeetingByAccountId.containsKey(aTask.AccountId)){
                previousMeetingByAccountId.put(aTask.AccountId, aTask);
            }
        }

        for(Task aTask : [SELECT id, Description, AccountId, ActivityDate, Subject, IsClosed FROM Task WHERE AccountId IN :allAccounts AND IsClosed = FALSE ORDER BY ActivityDate ASC]){
            if(!nextMeetingByAccountId.containsKey(aTask.AccountId)){
                nextMeetingByAccountId.put(aTask.AccountId, aTask);
            }
        }
        List<Id> accountIdsWithMeetings = new List<Id>();
        List<String> accountStringsWithMeetings = new List<String>();
        accountIdsWithMeetings.addAll(previousMeetingByAccountId.keySet());
        accountIdsWithMeetings.addAll(nextMeetingByAccountId.keySet());
        for(Id meetingId : accountIdsWithMeetings){
            accountStringsWithMeetings.add('\'' + meetingId + '\'');
        }
        if(accountStringsWithMeetings.size() > 0){
            baseQuery += ' AND  Id IN (' + String.join(accountStringsWithMeetings, ',') + ') ';
        }
        System.debug(baseQuery);
        System.debug(previousMeetingByAccountId);
        calculateAvailablePages();
        populateListOfPages();

        refreshQueryAndLoadTable();
    }
    public void initAccounts(){
        allAccounts =  Database.query(baseQuery);
    }
    public void populateAllOptions(){
        salesManagersOptions = new List<SelectOption>();
        teamLeadOptions = new List<SelectOption>();
        accountOptions = new List<SelectOption>();
        salesManagersOptions.add(new SelectOption('', '--None--'));
        teamLeadOptions.add(new SelectOption('', '--None--'));
        accountOptions.add(new SelectOption('', '--None--'));
        Set<Id> salesManagersIds = new Set<Id>();
        Set<Id> teamLeadsIds = new Set<Id>();
        for(Account acc : allAccounts){
            accountOptions.add(new SelectOption(acc.id, acc.Name));
            if(!salesManagersIds.contains(acc.OwnerId)){
                salesManagersIds.add(acc.OwnerId);
                salesManagersOptions.add(new SelectOption(acc.OwnerId, acc.Owner.Name));
            }
            if(acc.Owner.ManagerId != null && !teamLeadsIds.contains(acc.Owner.ManagerId)){
                teamLeadsIds.add(acc.Owner.ManagerId);
                teamLeadOptions.add(new SelectOption(acc.Owner.ManagerId, acc.Owner.Manager.Name));
            }
        }

    }
    public void sortHeader(){
        if(orderBy != null) {
            if (previousOrderBy != null && orderBy == previousOrderBy) {
                isAsc = !isAsc;
            } else {
                isAsc = true;
            }
            System.debug(orderBy);
            System.debug(previousOrderBy);
            previousOrderBy = orderBy;
            refreshQueryAndLoadTable();
        }
    }
    public void refreshQueryAndLoadTable(){
        String query = baseQuery;

        query += getFilters();

        if(orderBy != null) {
            query += getOrderBy();

        }
        System.debug(query);
        allAccounts = (List<Account>)Database.query(query);
        populateAllOptions();

        allMeetingWrappers.clear();
        currentPage = 1;
        populateAllMeetingWrappers();
        populateCurrentMeetingWrappers();
        calculateAvailablePages();
        populateListOfPages();
    }
    public String getFilters(){
        String filters = '';
        if(String.isNotBlank(managerIdChoice)){
            filters +=  ' AND OwnerId = \'' + String.escapeSingleQuotes(managerIdChoice) + '\'';
        }
        if(String.isNotBlank(teamLeadChoice)){
            filters +=  ' AND Owner.ManagerId = \'' + String.escapeSingleQuotes(teamLeadChoice) + '\'';
        }
        if(String.isNotBlank(accountChoice)){
            filters +=  ' AND Id = \'' + String.escapeSingleQuotes(accountChoice) + '\'';
        }
        return filters;
    }
    public String getOrderBy(){
        String orderByString;
        if(String.isNotBlank(orderBy)){
            orderByString = ' Order By ' + orderBy;
        }
        else{
            return '';
        }


        if(isAsc){
            orderByString += ' ASC';
        }
        else{
            orderByString += ' DESC';
        }
        orderByString += ' NULLS LAST';

        return orderByString;
    }


    public void populateAllMeetingWrappers(){

        for(Account acc : allAccounts){
            MeetingWrapper mw = new MeetingWrapper();
            mw.acc = acc;
            if(previousMeetingByAccountId.containsKey(acc.id)){
                mw.previousTask = previousMeetingByAccountId.get(acc.id);
            }
            if(nextMeetingByAccountId.containsKey(acc.id)){
                mw.nextTask = nextMeetingByAccountId.get(acc.id);
            }
            allMeetingWrappers.add(mw);
        }
    }
    public void populateCurrentMeetingWrappers(){
        currentMeetingWrappers = new List<MeetingWrapper>();
        for (Integer i = 0; i < ENTRIES;  i++) {
            if (i < allMeetingWrappers.size()) {
                currentMeetingWrappers.add(allMeetingWrappers.get(i));
            }
        }
    }
    public void calculateAvailablePages(){
        availablePages = allAccounts.size() / ENTRIES;
        if (Math.mod(allAccounts.size(),ENTRIES) > 0) {
            availablePages++;
        }

    }
    public void populateListOfPages(){
        pagesAvailable = new List<Integer>();
        for (Integer i = 0; i < availablePages; i++) {
            pagesAvailable.add(i);
        }
    }

    public PageReference nextPage() {
        if (currentPage + 1 <= pagesAvailable.size()) {
            currentMeetingWrappers.clear();
            currentPage++;
            for (Integer i = ENTRIES * currentPage - ENTRIES; i < ENTRIES * currentPage; i++) {
                if (i < allMeetingWrappers.size()) {
                    currentMeetingWrappers.add(allMeetingWrappers.get(i));
                }
            }
        }
        return null;
    }

    public PageReference previousPage() {
        if (currentPage - 1 > 0) {
            currentMeetingWrappers.clear();
            currentPage--;
            for (Integer i = ENTRIES * currentPage - ENTRIES; i < ENTRIES * currentPage; i++) {
                if (i < allAccounts.size()) {
                    currentMeetingWrappers.add(allMeetingWrappers.get(i));
                }
            }
        }
        return null;
    }

    public PageReference goToPage() {
        if (ApexPages.currentPage().getParameters().get('targetPage') != null) {
            targetPage = Integer.valueOf(ApexPages.currentPage().getParameters().get('targetPage'));
        }
        if (targetPage > 0 && targetPage <= pagesAvailable.size()) {
            currentMeetingWrappers.clear();
            currentPage = targetPage;
            for (Integer i = ENTRIES * currentPage - ENTRIES; i < ENTRIES * currentPage; i++) {
                if (i < allAccounts.size()) {
                    currentMeetingWrappers.add(allMeetingWrappers.get(i));
                }
            }
        }
        return null;
    }
    public void sortByMeeting(){
        targetPage = 1;
        currentPage = 1;
        System.debug(sortBy);
        if(previousOrderBy != null && orderBy == previousOrderBy){
            isAsc = !isAsc;
          //  reverseWrappers();
            System.debug(allMeetingWrappers);
        }
        else{

            isAsc = true;
        }
        meetingIsAsc = isAsc;
        allMeetingWrappers.sort();
        previousOrderBy = orderBy;
        populateCurrentMeetingWrappers();



    }
    public PageReference save(){
        try{
            List<Account> accountsToUpdate = new List<Account>();
            for(MeetingWrapper wrapper : allMeetingWrappers){
                accountsToUpdate.add(wrapper.acc);
            }
            update accountsToUpdate;
        }
        catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            System.debug(e.getMessage());
            System.debug(e.getStackTraceString());
            return null;
        }
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Updated!'));
        return null;
    }

    public PageReference cancel(){
        return new PageReference('/');
    }

    public class MeetingWrapper implements Comparable{
        public Account acc{get;set;}
        public Task previousTask{get;set;}
        public Task nextTask{get;set;}

        public Integer compareTo(Object compareTo){
            MeetingWrapper mwCompareTo = (MeetingWrapper) compareTo;
            if(sortBy == 'Previous'){
                if(previousTask == null ){
                    return 1;
                }
                else if(mwCompareTo.previousTask == null ){
                    return -1;
                }
                else if(previousTask.ActivityDate == mwCompareTo.previousTask.ActivityDate){
                    return 0;
                }
                else if(previousTask.ActivityDate > mwCompareTo.previousTask.ActivityDate){
                    if(meetingIsAsc == true){
                        return 1;
                    }
                    else{
                        return -1;
                    }
                }
                else{
                    if(meetingIsAsc == true){
                        return -1;
                    }
                    else{
                        return 1;
                    }
                }

            }
            else if(sortBy == 'Next'){
                if(nextTask == null){
                    return 1;
                }
                else if(mwCompareTo.nextTask == null ){
                    return -1;
                }
                else if(nextTask.ActivityDate == mwCompareTo.nextTask.ActivityDate){
                   return 0;
                }
                else if(nextTask.ActivityDate > mwCompareTo.nextTask.ActivityDate){
                    if(meetingIsAsc == true){
                        return 1;
                    }
                    else{
                        return -1;
                    }


                }
                else{
                    if(meetingIsAsc == true){
                        return -1;
                    }
                    else{
                        return 1;
                    }
                }

            }
            else{
                System.debug('invalid sortBY');
                return 0;
            }
        }
    }

    public PageReference exportCSV(){
        Integer rows = 0;
        String attachmentBody = '';
        final String PREV_MEETING_DATE = 'Previous Meeting Date';
        final String PREV_MEETING_DESC = 'Previous Meeting Description';
        final String NEXT_MEETING_DATE = 'Next Meeting Date';
        final String NEXT_MEETING_DESC = 'Next Meeting Description';
        final String OWNER = 'FCC Sales Rep';
        final String TEAM_LEAD = 'Team Lead';
        if(allMeetingWrappers == null || allMeetingWrappers.size() == 0){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Nothing to export'));
            return null;
        }

        List<String> headerValues = new List<String>{'Name', TEAM_LEAD,OWNER, 'Store_Count__c', 'BillingState', NEXT_MEETING_DATE, NEXT_MEETING_DESC,PREV_MEETING_DATE, PREV_MEETING_DESC,'Meeting_Comments__c'};

        attachmentBody += String.join(headerValues, ',');
        attachmentBody += '\n';
        for(MeetingWrapper mw : allMeetingWrappers){
            for(String field : headerValues){
                if(field == PREV_MEETING_DATE){
                    if(mw.previousTask != null && mw.previousTask.ActivityDate != null){
                        String value = mw.previousTask.ActivityDate.format();
                        value = value.escapeCsv();
                        attachmentBody += value;
                    }
                }
                else if(field == PREV_MEETING_DESC){
                    if(mw.previousTask != null && mw.previousTask.Description != null){
                        String value = mw.previousTask.Description;
                        value = value.escapeCsv();
                        attachmentBody += value;
                    }
                }
                else if(field == NEXT_MEETING_DATE){
                    if(mw.nextTask != null && mw.nextTask.ActivityDate != null){
                        String value = mw.nextTask.ActivityDate.format();
                        value = value.escapeCsv();
                        attachmentBody += value;
                    }
                }
                else if(field == NEXT_MEETING_DESC){
                    if(mw.nextTask != null && mw.nextTask.Description != null){
                        String value = mw.nextTask.Description;
                        value = value.escapeCsv();
                        attachmentBody += value;
                    }
                }
                else if(field == OWNER){
                    if(mw.acc.Owner.Name != null){
                        attachmentBody += mw.acc.Owner.Name.escapeCsv();
                    }
                }
                else if(field  == TEAM_LEAD){
                    if(mw.acc.Owner.ManagerId != null && String.isNotBlank(mw.acc.Owner.Manager.Name)){
                        attachmentBody += mw.acc.Owner.Manager.Name.escapeCsv();
                    }
                }
                else{
                    if(mw.acc.get(field) != null){
                        attachmentBody += String.valueOf(mw.acc.get(field)).escapeCsv();
                    }
                }
                attachmentBody += ',';
            }
            attachmentBody += '\n';
            rows++;
        }
        if(rows == 0){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'No rows to export'));
            return null;
        }
        System.debug(attachmentBody);
        Savepoint sp = Database.setSavepoint();
        try{
            CSV_Export__c csv_export = new CSV_Export__c();
            insert csv_export;
            Attachment att = new Attachment();
            att.Name = 'Meeting Tracker Export:' + Date.today().format() + '.csv';
            att.Body = Blob.valueOf(attachmentBody);
            att.ParentId = csv_export.id;
            att.ContentType = 'text/csv';
            insert att;

            return new PageReference('/servlet/servlet.FileDownload?file=' + att.id);
        }
        catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getMessage()));
            System.debug(e.getMessage());
            System.debug(e.getStackTraceString());
            Database.rollback(sp);
            return null;
        }
    }
}