/**
 * Created by ChrisMatos on 2/7/2017.
 */

public with sharing class DistributionHeatMapController {

    public List<Product2> productsOnHeatMap;

    public List<DistributionWrapper> mustStockWrappers{get;set;}
    public List<DistributionWrapper> shouldStockWrappers{get;set;}
    public Map<String, Integer> numMustProductsPerPackGroup{get;set;}
    public Map<String, Integer> numShouldProductsPerPackGroup{get;set;}
    public String viewSelection{get;set;}
    public String teamLeadMustChoice{get;set;}
    public String teamLeadShouldChoice{get;set;}
    public String accountMustChoice{get;set;}
    public String accountShouldChoice{get;set;}
    public Map<String,List<SelectOption>> teamLeadOptionsbyStock{get;set;}
    public Map<String,List<SelectOption>> accountOptionsbyStock{get;set;}
    private List<Opportunity> oppsInHeatMap;
    
    public static final String MUST_STOCK = 'Must';
    public static final String SHOULD_STOCK = 'Should';
    public Map<String, List<Account>> accountsByStock{get;set;}
    private Map<String, Set<Id>> accountIdsByStock;
    private List<String> stocks;

    public DistributionHeatMapController(){
//        stocks = new List<String>();
//        List<Schema.PicklistEntry> ples = Product2.Stock__c.getDescribe().getPicklistValues();
//
//        for(PicklistEntry ple : ples){
//            stocks.add(ple.getValue());
//        }
        stocks = new List<String>{MUST_STOCK, SHOULD_STOCK};
        productsOnHeatMap = [SELECT id, Stock__c, Description, Name, Item_Code__c, Heat_Map_Group__c, Heat_Map_Pack_Group__c FROM Product2 WHERE (Stock__c = :MUST_STOCK OR Stock__c = :SHOULD_STOCK) AND IsActive = TRUE AND Heat_Map_Pack_Group__c != NULL AND Heat_Map_Group__c != NULL Order BY Heat_Map_Pack_Group__c ASC];
        numMustProductsPerPackGroup = new Map<String, Integer>();
        numShouldProductsPerPackGroup = new Map<String, Integer>();
        accountsByStock = new Map<String, List<Account>>();
        for(String stock : stocks){
            accountsByStock.put(stock, new List<Account>());

        }


        viewSelection = MUST_STOCK;

        Map<String,Map<String, Map<String, DistributionWrapper>>> wrappersByProductNameByPackByStock = new Map<String,Map<String, Map<String, DistributionWrapper>>>();
        for(String stock : stocks){
            wrappersByProductNameByPackByStock.put(stock, new Map<String, Map<String, DistributionWrapper>>());
        }

        List<Schema.PicklistEntry> pleEntries = Product2.Heat_Map_Pack_Group__c.getDescribe().getPicklistValues();
        for(PicklistEntry ple : pleEntries){
            for(String stock : stocks){
                wrappersByProductNameByPackByStock.get(stock).put(ple.getValue(), new Map<String, DistributionWrapper>());
            }
            numMustProductsPerPackGroup.put(ple.getValue(), 0);
            numShouldProductsPerPackGroup.put(ple.getValue(), 0);

            
        }
        if(productsOnHeatMap == null || productsOnHeatMap.size() == 0){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'No Products to Display'));
            System.debug('No Products');
            return;
        }


        for(Product2 prod : productsOnHeatMap){
            if(wrappersByProductNameByPackByStock.get(prod.Stock__c).get(prod.Heat_Map_Pack_Group__c).containsKey(prod.Heat_Map_Group__c)){
                wrappersByProductNameByPackByStock.get(prod.Stock__c).get(prod.Heat_Map_Pack_Group__c).get(prod.Heat_Map_Group__c).productIds.add(prod.id);
            }
            else{
                DistributionWrapper msw = new DistributionWrapper(prod.Heat_Map_Group__c);
                msw.productIds.add(prod.id);
                wrappersByProductNameByPackByStock.get(prod.Stock__c).get(prod.Heat_Map_Pack_Group__c).put(prod.Heat_Map_Group__c, msw);
                if(prod.Stock__c == MUST_STOCK){
                    if(numMustProductsPerPackGroup.containsKey(prod.Heat_Map_Pack_Group__c)){
                        Integer newNumProducts = numMustProductsPerPackGroup.get(prod.Heat_Map_Pack_Group__c);
                        if(newNumProducts != null){
                            numMustProductsPerPackGroup.put(prod.Heat_Map_Pack_Group__c,newNumProducts + 1);
                        }

                    }else{
                        System.debug('Unknown pack: ' + prod.Heat_Map_Pack_Group__c);
                    }
                }
                else if(prod.Stock__c == SHOULD_STOCK){
                    if(numShouldProductsPerPackGroup.containsKey(prod.Heat_Map_Pack_Group__c)){
                        Integer newNumProducts = numShouldProductsPerPackGroup.get(prod.Heat_Map_Pack_Group__c);
                        if(newNumProducts != null){
                            numShouldProductsPerPackGroup.put(prod.Heat_Map_Pack_Group__c,newNumProducts + 1);
                        }

                    }else{
                        System.debug('Unknown pack: ' + prod.Heat_Map_Pack_Group__c);
                    }
                }

            }
        }
        oppsInHeatMap = [SELECT id,IsWon,StageName, AccountId,TDP__c, Account.Name, Item__c,Item__r.Stock__c, Item__r.Heat_Map_Pack_Group__c, Item__r.Heat_Map_Group__c, Item_Description__c FROM Opportunity WHERE Item__c IN :productsOnHeatMap];

        accountIdsByStock = new Map<String,Set<Id>>();
        for(String stock : stocks){
            accountIdsByStock.put(stock, new Set<Id>());
        }
        for(Opportunity opp : oppsInHeatMap) {
            if(opp.Item__r.Stock__c == MUST_STOCK || opp.Item__r.Stock__c == SHOULD_STOCK) {
                accountIdsByStock.get(opp.Item__r.Stock__c).add(opp.AccountId);
            }

        }
        populateAccounts();

        for(String stock : stocks){
            for(Account acc : accountsByStock.get(stock)){
                for(String key : wrappersByProductNameByPackByStock.get(stock).keySet()){

                    for(DistributionWrapper dw : wrappersByProductNameByPackByStock.get(stock).get(key).values()){
                        dw.leTDPByAccountId.put(acc.Id, 0);
                        dw.pendingTDPByAccountId.put(acc.id, 0);
                    }

                }
            }
        }
        for(Opportunity opp : oppsInHeatMap){
            if(String.isNotBlank(opp.Item__r.Heat_Map_Group__c) &&
                    wrappersByProductNameByPackByStock.get(opp.Item__r.Stock__c).get(opp.Item__r.Heat_Map_Pack_Group__c).containsKey(opp.Item__r.Heat_Map_Group__c)){
                DistributionWrapper wrapper = wrappersByProductNameByPackByStock.get(opp.Item__r.Stock__c).get(opp.Item__r.Heat_Map_Pack_Group__c).get(opp.Item__r.Heat_Map_Group__c);
                if(opp.IsWon){
                    if(wrapper.leTDPByAccountId.containsKey(opp.AccountId)){
                        Decimal total = opp.TDP__c == null ? wrapper.leTDPByAccountId.get(opp.AccountId) : wrapper.leTDPByAccountId.get(opp.AccountId) + opp.TDP__c;
                        wrapper.leTDPByAccountId.put(opp.AccountId, total);
                    }
                }
                else if(opp.StageName == 'Pending'){
                    if(wrapper.pendingTDPByAccountId.containsKey(opp.AccountId)){
                        Decimal total = opp.TDP__c == null ? wrapper.pendingTDPByAccountId.get(opp.AccountId) : wrapper.pendingTDPByAccountId.get(opp.AccountId) + opp.TDP__c;
                        wrapper.pendingTDPByAccountId.put(opp.AccountId, total);
                    }
                }


            }
        }

        mustStockWrappers = new List<DistributionWrapper>();
        for(String key : wrappersByProductNameByPackByStock.get(MUST_STOCK).keySet()){
            mustStockWrappers.addAll(wrappersByProductNameByPackByStock.get(MUST_STOCK).get(key).values());
        }

        shouldStockWrappers = new List<DistributionWrapper>();
        for(String key : wrappersByProductNameByPackByStock.get(SHOULD_STOCK).keySet()){
            shouldStockWrappers.addAll(wrappersByProductNameByPackByStock.get(SHOULD_STOCK).get(key).values());
        }

        System.debug(wrappersByProductNameByPackByStock.get(SHOULD_STOCK));
        teamLeadOptionsbyStock = new Map<String, List<SelectOption>>();
        accountOptionsbyStock = new Map<String, List<SelectOption>>();
        for(String stock : stocks){
            teamLeadOptionsbyStock.put(stock, new List<SelectOption>());
            accountOptionsbyStock.put(stock, new List<SelectOption>());
        }
        Set<Id> managerIds = new Set<Id>();
        for(String stock : stocks){
            for(Account acc : accountsByStock.get(stock)){
                if(acc.Owner.ManagerId != null && !managerIds.contains(acc.Owner.ManagerId)){
                    teamLeadOptionsbyStock.get(stock).add(new SelectOption(acc.Owner.ManagerId, acc.Owner.Manager.Name));
                    managerIds.add(acc.Owner.ManagerId);
                }
                accountOptionsbyStock.get(stock).add(new SelectOption(acc.id, acc.Name));

            }
        }
        for(String stock : stocks) {
            if(teamLeadOptionsbyStock.get(stock).size() > 0){
                teamLeadOptionsbyStock.put(stock, Utils.selectOptionSortByLabel(teamLeadOptionsbyStock.get(stock)));
                teamLeadOptionsbyStock.get(stock).add(0,new SelectOption('', '--None--'));
            }
            else{
                teamLeadOptionsbyStock.get(stock).add(new SelectOption('', '--None--'));
            }
           if(accountOptionsbyStock.get(stock).size() > 0){
               accountOptionsbyStock.put(stock, Utils.selectOptionSortByLabel(accountOptionsbyStock.get(stock)));
               accountOptionsbyStock.get(stock).add(0,new SelectOption('', '--None--') );
           }
            else{
                accountOptionsbyStock.get(stock).add(new SelectOption('', '--None--'));
            }

        }


    }
    public void populateAccounts(){
        for(String stock : stocks){
            accountsByStock.put(stock, getFilteredAccounts(accountIdsByStock.get(stock), stock));
        }
    }
    public void changeStockView(){
        accountsByStock.put(viewSelection, getFilteredAccounts(accountIdsByStock.get(viewSelection), viewSelection));
    }
    public List<Account> getFilteredAccounts(Set<Id> accountIds, String stock ){
        String orderBy = ' Order By Name Asc';
        String query = 'SELECT id, Name, OwnerId, Owner.ManagerId, Owner.Manager.Name FROM Account';
        if(stock == MUST_STOCK){
            if(String.isNotBlank(accountMustChoice)){
                query += ' WHERE Id = \'' + accountMustChoice + '\'';
                query += orderBy;
                System.debug(query);
                return Database.query(query);
            }
        }
        else if(stock == SHOULD_STOCK){
            if(String.isNotBlank(accountShouldChoice)){
                query += ' WHERE Id = \'' + accountShouldChoice + '\'';
                query += orderBy;
                System.debug(query);
                return Database.query(query);
            }
        }
        List<String> accountIdsAsStrings = new List<String>();
        for(Id accId : accountIds){
            accountIdsAsStrings.add('\'' + accId + '\'');
        }
        if(accountIdsAsStrings.size() == 0){
            return new List<Account>();
        }
        query += ' WHERE Id IN (' + String.join(accountIdsAsStrings, ',') + ') ';
        if(stock == MUST_STOCK) {
            if (String.isNotBlank(teamLeadMustChoice)) {
                query += 'AND Owner.ManagerId = \'' + String.escapeSingleQuotes(teamLeadMustChoice) + '\'';
            }
        }
        else if(stock == SHOULD_STOCK){
            if (String.isNotBlank(teamLeadShouldChoice)) {
                query += 'AND Owner.ManagerId = \'' + String.escapeSingleQuotes(teamLeadShouldChoice) + '\'';
            }
        }
        query += orderBy;

        return Database.query(query);
    }
}